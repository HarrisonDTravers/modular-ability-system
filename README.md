# Modular Ability System (MAS)

## What is an Ability?
To provide context of what the word "Ability" means in Modular Ability system I would like to first like to say the word has been loaned from the common usage in Multiplayer Online Battle Arena (MobA) Games.
In a MobA, An ability refers to a set of behaviours that can be executed by a player-controlled character during combat. Abilities are usually specific to each character and can range from basic attacks to complex spells or actions.

Abilities typically have defined parameters, including targeting type, range, damage or healing values, and other effects such as buffs or debuffs. Some abilities can also modify the game state, such as by granting temporary invincibility or by changing the position of the character.

Each ability is usually tied to a unique input command, such as a keyboard key or mouse click, and can be activated using the relevant input. Abilities often have resource requirements, such as a cooldown period, mana cost, or other limiting factors that determine how frequently they can be used.

## What does MAS do?
It provides structured system and an array of behaviours that users can choose from to rapidly and reliably develop abilities.

## What does the system look like?
For those who are interested in seeing the architecture of the library. Please have a peek at the UML below.
As you'll quickly notice, there is a strong use of Object Orientation. Via Inheritance, aggregation and interface implementation. The system primarily uses the design pattern known as the strategy pattern to attach behaviours to different parts of the main structure.

https://pasteboard.co/um6bWg931Vpu.png

Please keep in mind that this is a minimized and described version of the system for the purpose of understanding general structure.. 

## Where do I get it?
Because MAS is a hobby project, it can only be worked on it in free time. I have my ideals and it isn't quite in the state I want it yet. I would expect it to be able to be used to create most abilities in known MobAs before I release an initial version.

## License

[AGPL-3.0](https://choosealicense.com/licenses/agpl-3.0/)