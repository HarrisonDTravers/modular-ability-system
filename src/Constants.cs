﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public static class Constants
    {
        public delegate void delVoidInputVector3(Vector3 vector3);
        public delegate void delVoidInputVector2(Vector2 vector2);
        public delegate void delVoidInputVector2Int(Vector2Int vector2);
        public delegate void delVoidInputTileType(TileType tileType);
        public delegate void delVoidInputInt(int i);
        public delegate void delVoidInputFloat(float f);
        public delegate void delVoidEffectContext(EffectContext context);
        public delegate void delVoidInputInt_EffectContext(int i, EffectContext context);
        public delegate void delVoidInputFloat_EffectContext(float f, EffectContext context);
        public delegate void delVoidInputBool(bool boolean);
        public delegate void delVoidInputCastBlocker(CastBlocker castBlocker);
        public delegate void delVoidInputEntityAdapter(EntityAdapter entityAdapter);
        public delegate void delVoidInputEntityAdapter_EffectContext(EntityAdapter entityAdapter, EffectContext effectContext);
        public delegate void delVoidInputEntityAdapter_TransformMimic(EntityAdapter entityAdapter, TransformMimic transformMimic);
        public delegate void delVoid();
        public delegate bool delBool();
        public delegate float delFloat();
        public delegate Rigidbody delRigidbody();
        public delegate Vector3 delVector3();
        public delegate T delT<T>();

        public static class UI
        {
            public static Material SpritesDefault { get { return new Material(Shader.Find("Sprites/Default")); } }
        }
    }
}
