﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{

    public interface IOnCastTypeCastBehaviour : ICloneable
    {
        void OnCast(AbilityContext abilityContext);
    }

    public class PlaySoundOnEntityOnCastTypeCast : IOnCastTypeCastBehaviour
    {
        public AudioClip AudioClip { get; set; }
        public AbilityContext.AbilityContextEntities Entity { get; set; }

        public PlaySoundOnEntityOnCastTypeCast(AudioClip audioClip, AbilityContext.AbilityContextEntities Entity)
        {
            AudioClip = audioClip;
        }

        public void OnCast(AbilityContext abilityContext)
        {
            AudioSource audioSource = new GameObject("Temporary AudioSource").AddComponent<AudioSource>();
            audioSource.transform.SetParent(abilityContext.Get(Entity).transform);
            audioSource.clip = AudioClip;
            audioSource.Play();
            GameObject.Destroy(audioSource.gameObject, 1+AudioClip.length);
        }

        public PlaySoundOnEntityOnCastTypeCast Clone()
        {
            return new PlaySoundOnEntityOnCastTypeCast(AudioClip, Entity);
        }

        public ICloneable IClone()
        {
            return Clone();
        }
    }

    public class ParticleSystemOnEntityOnCastTypeCastBehaviour : IOnCastTypeCastBehaviour
    {
        //Replace all these with LineRendererBehaviours
        public ParticleSystem ParticleSystem { get; set; }
        public AbilityContext.AbilityContextEntities ContextType { get; set; }
        public float DistanceFromEntity { get; set; }

        public ParticleSystemOnEntityOnCastTypeCastBehaviour(AbilityContext.AbilityContextEntities contextType, ParticleSystem particleSystem)
        {
            ParticleSystem = particleSystem;
            ContextType = contextType;
        }

        public ParticleSystemOnEntityOnCastTypeCastBehaviour(AbilityContext.AbilityContextEntities contextType, ParticleSystem particleSystem, float distanceFromEntity)
        {
            ParticleSystem = particleSystem;
            ContextType = contextType;
            DistanceFromEntity = distanceFromEntity;
        }

        public void OnCast(AbilityContext abilityContext)
        {
            ParticleSystem instance = MonoBehaviour.Instantiate<ParticleSystem>(ParticleSystem);
            instance.gameObject.transform.SetParent(abilityContext.Get(ContextType).transform);

            float rotation = abilityContext.Get(ContextType).transform.rotation.y; //y coorinate is rotation in 3D space
            Vector3 rotationAsUnitVector = Utilities.AngleToVector(Vector3.forward * DistanceFromEntity, Mathf.Deg2Rad * rotation).normalized;
            Vector3 boxPosition = abilityContext.Get(ContextType).transform.position + rotationAsUnitVector;

            instance.transform.position = boxPosition;
            instance.transform.localRotation = Quaternion.Euler(-90,90,90);

            MonobehaviourReference.Instance.StartCoroutine(DestroyAfterLifetime(instance));
        }

        public IEnumerator DestroyAfterLifetime(ParticleSystem particleSystem)
        {
            bool lifeEnded = false;
            float timePassed = 0;
            while (!lifeEnded)
            {
                if (timePassed >= particleSystem.main.duration + particleSystem.main.startLifetime.constantMax) lifeEnded = true;
                timePassed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            GameObject.Destroy(particleSystem.gameObject);
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public ParticleSystemOnEntityOnCastTypeCastBehaviour Clone()
        {
            return new ParticleSystemOnEntityOnCastTypeCastBehaviour(ContextType, ParticleSystem);
        }
    }
}
