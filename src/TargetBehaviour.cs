﻿#define inDevelopment

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MAS
{
    public abstract class TargetBehaviour : ICloneable
    {
        public UnitMatrix UnitMatrix { get; set; }

        public TargetBehaviour(UnitMatrix unitMatrix)
        {
            UnitMatrix = unitMatrix;
        }

        public abstract List<EntityAdapter> GetTargets(AbilityContext abilityContext);
        public abstract ICloneable IClone();
        public abstract TargetBehaviour Clone();
    }

    public class SelfTargetBehaviour : TargetBehaviour
    {
        private SelfTargetBehaviour(UnitMatrix unitMatrix) : base(unitMatrix)
        {
        }

        public SelfTargetBehaviour() : base(UnitMatrix.SelfBeastling())
        {

        }

        public override TargetBehaviour Clone()
        {
            return new SelfTargetBehaviour();
        }

        public override List<EntityAdapter> GetTargets(AbilityContext abilityContext)
        {
            List<EntityAdapter> eA = new List<EntityAdapter>();
            eA.Add(abilityContext.Sender);
            return eA;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public class AreaTargetBehaviour : TargetBehaviour
    {
        public int MaxTargets { get; set; }
        public AreaShape AreaShape { get; set; }

        public AreaTargetBehaviour(UnitMatrix unitMatrix, int maxTargets, float range) : base(unitMatrix)
        {
            AreaShape = new SphereAreaShape(AbilityContext.AbilityContextTransforms.Sender, range, 0, 0);
            MaxTargets = maxTargets;
        }

        public AreaTargetBehaviour(AreaShape areaShape, UnitMatrix unitMatrix, int maxTargets) : base(unitMatrix)
        {
            AreaShape = areaShape;
            MaxTargets = maxTargets;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override TargetBehaviour Clone()
        {
            return new AreaTargetBehaviour(AreaShape.Clone(), UnitMatrix.Clone(), MaxTargets);
        }

        public override List<EntityAdapter> GetTargets(AbilityContext abilityContext)
        {
            Collider[] hits = AreaShape.GetHits(abilityContext);

#if inDevelopment
            if (DebugHandler.VerboseTargetBehaviours == true)
                Debug.Log(hits.Length + " objects in range");
#endif

            List<EntityAdapter> hitEntities = new List<EntityAdapter>();
            foreach (Collider hit in hits)
            {
                EntityAdapter hitEntity = hit.GetComponent<EntityAdapter>();
                if (hitEntity != null)
                    hitEntities.Add(hitEntity);
            }
            
#if inDevelopment
            if (DebugHandler.VerboseTargetBehaviours == true)
                Debug.Log(hitEntities.Count + " entities in range");
#endif

            List<EntityAdapter> targets = new List<EntityAdapter>();
            foreach (EntityAdapter hitEntity in hitEntities)
            {
                if (hitEntity != null && UnitMatrix.CanTarget(abilityContext.Sender, hitEntity))
                {
                    targets.Add(hitEntity);
                }
            }

#if inDevelopment
            if (DebugHandler.VerboseTargetBehaviours == true)
                Debug.Log(targets.Count + " targetable entities in range");
#endif

            targets = targets.OrderBy(x => Vector3.Distance(x.transform.position, abilityContext.Get(AreaShape.RelativeTo, TransformVectorTypes.Position))).ToList();

            //TODO : Target list modification behaviour call here, defaulting to closest n units

            return MaxTargets > 0 ? targets.Take(MaxTargets).ToList() : targets;
        }
    }

    public class Area2DTargetBehaviour : TargetBehaviour
    {
        public int MaxTargets { get; set; }
        public AreaShape2D AreaShape { get; set; }

        public Area2DTargetBehaviour(UnitMatrix unitMatrix, int maxTargets, float range) : base(unitMatrix)
        {
            AreaShape = new CircleAreaShape(AbilityContext.AbilityContextTransforms.Sender, range, 0, 0);
            MaxTargets = maxTargets;
        }

        public Area2DTargetBehaviour(AreaShape2D areaShape, UnitMatrix unitMatrix, int maxTargets) : base(unitMatrix)
        {
            AreaShape = areaShape;
            MaxTargets = maxTargets;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override TargetBehaviour Clone()
        {
            return new Area2DTargetBehaviour(AreaShape.Clone(), UnitMatrix.Clone(), MaxTargets);
        }

        public override List<EntityAdapter> GetTargets(AbilityContext abilityContext)
        {
            RaycastHit2D[] hits = AreaShape.GetHits(abilityContext);


#if inDevelopment
            if (DebugHandler.VerboseTargetBehaviours == true)
                Debug.Log(hits.Length + " objects in range");
#endif

            List<EntityAdapter> hitEntities = new List<EntityAdapter>();
            foreach (RaycastHit2D hit in hits)
            {
                EntityAdapter hitEntity = hit.collider.GetComponent<EntityAdapter>();
                if (hitEntity != null)
                    hitEntities.Add(hitEntity);
            }

#if inDevelopment
            if (DebugHandler.VerboseTargetBehaviours == true)
                Debug.Log(hitEntities.Count + " entities in range");
#endif

            List<EntityAdapter> targets = new List<EntityAdapter>();
            foreach (EntityAdapter hitEntity in hitEntities)
            {
                if (hitEntity != null && UnitMatrix.CanTarget(abilityContext.Sender, hitEntity))
                {
                    targets.Add(hitEntity);
                }
            }

#if inDevelopment
            if (DebugHandler.VerboseTargetBehaviours == true)
                Debug.Log(targets.Count + " targetable entities in range");
#endif

            targets = targets.OrderBy(x => Vector3.Distance(x.transform.position, abilityContext.Get(AreaShape.RelativeTo, TransformVectorTypes.Position))).ToList();

            //TODO : Target list modification behaviour call here, defaulting to closest n units

            return MaxTargets > 0 ? targets.Take(MaxTargets).ToList() : targets;
        }
    }

    public abstract class AreaShape : ICloneable
    {
        public AbilityContext.AbilityContextTransforms RelativeTo { get; set; }
        public float RotationOffset { get; set; }
        public float DistanceFromRelative { get; set; }

        public AreaShape(AbilityContext.AbilityContextTransforms relativeTo, float rotationOffset, float distanceFromRelative)
        {
            RelativeTo = relativeTo;
            RotationOffset = rotationOffset;
            DistanceFromRelative = distanceFromRelative;
        }

        public abstract Collider[] GetHits(AbilityContext abilityContext);
        public abstract AreaShape Clone();
        public abstract ICloneable IClone();
    }

    public abstract class AreaShape2D : ICloneable
    {
        public AbilityContext.AbilityContextTransforms RelativeTo { get; set; }
        public float RotationOffset { get; set; }
        public float DistanceFromRelative { get; set; }

        public AreaShape2D(AbilityContext.AbilityContextTransforms relativeTo, float rotationOffset, float distanceFromRelative)
        {
            RelativeTo = relativeTo;
            RotationOffset = rotationOffset;
            DistanceFromRelative = distanceFromRelative;
        }

        public abstract RaycastHit2D[] GetHits(AbilityContext abilityContext);
        public abstract AreaShape2D Clone();
        public abstract ICloneable IClone();
    }

    public class CircleAreaShape : AreaShape2D
    {
        public float Radius { get; set; }

        public CircleAreaShape(AbilityContext.AbilityContextTransforms relativeTo, float radius, float rotationOffset, float distanceFromRelative) : base(relativeTo, rotationOffset, distanceFromRelative)
        {
            Radius = radius;
        }

        public CircleAreaShape(CircleAreaShape shape) : base(shape.RelativeTo, shape.RotationOffset, shape.DistanceFromRelative)
        {
            Radius = shape.Radius;
        }

        public override RaycastHit2D[] GetHits(AbilityContext abilityContext)
        {
            float rotation = abilityContext.Get(RelativeTo, TransformVectorTypes.Rotation).z; //Z coorinate is rotation in 2D space
            Vector2 rotationAsUnitVector = Utilities.AngleToVector(Vector2.up * DistanceFromRelative, Mathf.Deg2Rad * rotation).normalized;
            Vector2 circlePosition = (Vector2)abilityContext.Get(RelativeTo, TransformVectorTypes.Position) + rotationAsUnitVector;
            return Physics2D.CircleCastAll(circlePosition, Radius, Vector2.zero);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override AreaShape2D Clone()
        {
            return new CircleAreaShape(this);
        }
    }

    public class RectangleAreaShape : AreaShape2D
    {
        public float Width { get; set; }
        public float Height { get; set; }

        public RectangleAreaShape(float width, float height, AbilityContext.AbilityContextTransforms relativeTo, float rotationOffset, float distanceFromRelative) : base(relativeTo, rotationOffset, distanceFromRelative)
        {
            Width = width;
            Height = height;
        }

        public RectangleAreaShape(RectangleAreaShape shape) : base(shape.RelativeTo, shape.RotationOffset, shape.DistanceFromRelative)
        {
            Width = shape.Width;
            Height = shape.Height;
        }

        public override RaycastHit2D[] GetHits(AbilityContext abilityContext)
        {
            float rotation = abilityContext.Get(RelativeTo, TransformVectorTypes.Rotation).z; //Z coorinate is rotation in 2D space
            Vector2 rotationAsUnitVector = Utilities.AngleToVector(Vector2.up * DistanceFromRelative, Mathf.Deg2Rad * rotation).normalized;
            Vector2 boxPosition = (Vector2)abilityContext.Get(RelativeTo, TransformVectorTypes.Position) + rotationAsUnitVector;
            DebugHandler.BoxCast2D(boxPosition, new Vector2(Width, Height), rotation, Vector2.zero, 0);
            return Physics2D.BoxCastAll(boxPosition, new Vector2(Width, Height), rotation, Vector2.zero);
        }

        public override AreaShape2D Clone()
        {
            return new RectangleAreaShape(this);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public class SphereAreaShape : AreaShape
    {
        public float Radius { get; set; }

        public SphereAreaShape(AbilityContext.AbilityContextTransforms relativeTo, float radius, float rotationOffset, float distanceFromRelative) : base(relativeTo, rotationOffset, distanceFromRelative)
        {
            Radius = radius;
        }

        public SphereAreaShape(SphereAreaShape shape) : base(shape.RelativeTo, shape.RotationOffset, shape.DistanceFromRelative)
        {
            Radius = shape.Radius;
        }

        public override Collider[] GetHits(AbilityContext abilityContext)
        {
            float rotation = abilityContext.Get(RelativeTo, TransformVectorTypes.Rotation).y;
            Vector2 rotationAsUnitVector = Utilities.AngleToVector(Vector3.forward * DistanceFromRelative, Mathf.Deg2Rad * rotation).normalized;
            Vector3 circlePosition = abilityContext.Get(RelativeTo, TransformVectorTypes.Position) + new Vector3(rotationAsUnitVector.x, 0, rotationAsUnitVector.y);
            //DebugHandler.DrawWireSphere(circlePosition, Radius, Color.red, 5); dodgy af
            return Physics.OverlapSphere(circlePosition, Radius);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override AreaShape Clone()
        {
            return new SphereAreaShape(this);
        }
    }

    public class BoxAreaShape : AreaShape
    {
        public float Width { get; set; }
        public float Length { get; set; }
        public float Height { get; set; }

        public BoxAreaShape(float width, float height, float length, AbilityContext.AbilityContextTransforms relativeTo, float rotationOffset, float distanceFromRelative) : base(relativeTo, rotationOffset, distanceFromRelative)
        {
            Width = width;
            Height = height;
            Length = length;
        }

        public BoxAreaShape(BoxAreaShape shape) : base(shape.RelativeTo, shape.RotationOffset, shape.DistanceFromRelative)
        {
            Width = shape.Width;
            Height = shape.Height;
        }

        public override Collider[] GetHits(AbilityContext abilityContext)
        {
            float rotation = abilityContext.Get(RelativeTo, TransformVectorTypes.Rotation).z; //Z coorinate is rotation in 2D space
            Vector2 rotationAsUnitVector = Utilities.AngleToVector(Vector2.up * DistanceFromRelative, Mathf.Deg2Rad * rotation).normalized;
            Vector2 boxPosition = (Vector2)abilityContext.Get(RelativeTo, TransformVectorTypes.Position) + rotationAsUnitVector;
            DebugHandler.BoxCast2D(boxPosition, new Vector3(Width, Height, Length), rotation, Vector2.zero, 0);
            return Physics.BoxCastAll(boxPosition, new Vector3(Width, Height, Length), Vector3.zero, Quaternion.Euler(0, rotation, 0)).Select((RaycastHit hit) => hit.collider).ToArray();
        }

        public override AreaShape Clone()
        {
            return new BoxAreaShape(this);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public class CustomAreaShape : AreaShape
    {
        //Constructor Fields
        private Vector2[] points;
        public Vector2[] Points { get { return points; } set { ProcessPoints(value); } }
        public Vector2 Pivot;
        public float PivotRotation;
        public float PivotRotationAroundCenter;
        public Vector2 PivotPositionRelativeToCenter;
        public float Height { get; set; }

        public List<ICustomShapeBehaviour> Behaviours { get; set; }

        //Calculated values
        private Vector2[] pointPositionsAfterSetup;
        private Vector3 boxSize;
        private Vector2 boxDisplacement;

        public CustomAreaShape(AbilityContext.AbilityContextTransforms relativeTo, float distanceFromRelative, Vector2[] points) : base(relativeTo, 0, distanceFromRelative)
        {
            Points = points;
            Behaviours = new List<ICustomShapeBehaviour>();
            Height = 1;
        }

        public CustomAreaShape(AbilityContext.AbilityContextTransforms relativeTo, float distanceFromRelative, Vector2[] points, float height, params ICustomShapeBehaviour[] customShapeBehaviours) : base(relativeTo, 0, distanceFromRelative)
        {
            Points = points;
            Behaviours = customShapeBehaviours.ToList();
            Height = height;
        }

        public CustomAreaShape(CustomAreaShape shape) : base(shape.RelativeTo, shape.RotationOffset, shape.DistanceFromRelative)
        {
            Behaviours = Utilities.CloneList(shape.Behaviours);
            Vector2[] points = new Vector2[shape.Points.Length];
            for (int ii = 0; ii < shape.Points.Length; ii++)
                points[ii] = new Vector2(shape.Points[ii].x, shape.Points[ii].y);

            Points = points;
        }

        public void ProcessPoints(Vector2[] points)
        {
            this.points = points;

            pointPositionsAfterSetup = RelatePointsToPosition(points, Pivot, PivotRotation, PivotRotationAroundCenter, PivotPositionRelativeToCenter);
            GetBoundingBox(pointPositionsAfterSetup, out Vector2 bottomLeftMostPoint, out Vector2 topRightMostPoint);
            Vector2 boxSize2D = topRightMostPoint - bottomLeftMostPoint;
            boxSize =  new Vector3(boxSize2D.x, Height, boxSize2D.y);
            boxDisplacement = (bottomLeftMostPoint + topRightMostPoint) / 2;
        }

        //Gets the greatest individual direction for up, down, left, right from each point. Vector[0] is width, Vector[1] is height
        public void GetBoundingBox(Vector2[] points, out Vector2 bottomLeftMostPoint, out Vector2 topRightMostPoint)
        {
            Vector2[] boundingBoxPoints = new Vector2[4];

            float xMin = float.MaxValue;
            float xMax = float.MinValue;
            float yMin = float.MaxValue;
            float yMax = float.MinValue;

            for (int ii = 0; ii < points.Length; ii++)
            {
                if (points[ii].x < xMin) xMin = points[ii].x;
                if (points[ii].x > xMax) xMax = points[ii].x;
                if (points[ii].y < yMin) yMin = points[ii].y;
                if (points[ii].y > yMax) yMax = points[ii].y;
            }

            boundingBoxPoints[0] = new Vector2(xMin, yMin);
            boundingBoxPoints[1] = new Vector2(xMin, yMax);
            boundingBoxPoints[2] = new Vector2(xMax, yMax);
            boundingBoxPoints[3] = new Vector2(xMax, yMin);

            bottomLeftMostPoint = boundingBoxPoints[0];
            topRightMostPoint = boundingBoxPoints[2];
        }

        public Vector2[] RelatePointsToPosition(Vector2[] points, Vector2 pointPivot, float pivotRotation, float pivotsRotationAroundCenter, Vector2 pivotPositionRelativeToCenter)
        {
            Vector2[] pointsRelativePositionToPivot = new Vector2[points.Length];
            Vector2[] pointsPositionRelativeToPlayer = new Vector2[points.Length];

            for (int ii = 0; ii < points.Length; ii++)
            {
                //Points position relative to Pivots
                pointsRelativePositionToPivot[ii] = points[ii] - pointPivot;

                //Apply rotation of where pivot is facing
                pointsRelativePositionToPivot[ii] = RotatePointAroundPoint(Vector3.zero, pivotRotation, pointsRelativePositionToPivot[ii]);

                //Apply Pivots position relative to player
                pointsPositionRelativeToPlayer[ii] = pointsRelativePositionToPivot[ii] + pivotPositionRelativeToCenter;

                // Apply Pivots rotation relative to player
                pointsPositionRelativeToPlayer[ii] = RotatePointAroundPoint(Vector3.zero, pivotsRotationAroundCenter, pointsPositionRelativeToPlayer[ii]);
            }

            return pointsPositionRelativeToPlayer;
        }

        public bool ContainsPoint(Vector2[] points, Vector3 p3D)
        {
            var p = new Vector2(p3D.x, p3D.z);

            var j = points.Length - 1;
            var inside = false;
            for (int i = 0; i < points.Length; j = i++)
            {
                var pi = points[i];
                var pj = points[j];
                Debug.DrawLine(pi, pj, Color.magenta);
                if (((pi.y <= p.y && p.y < pj.y) || (pj.y <= p.y && p.y < pi.y)) &&
                    (p.x < (pj.x - pi.x) * (p.y - pi.y) / (pj.y - pi.y) + pi.x))
                    inside = !inside;
            }
            return inside;
        }

        Vector2 RotatePointAroundPoint(Vector2 pivotPoint, float angle, Vector2 orbitingPoint)
        {
            Vector2 orbitedPoint = new Vector2(orbitingPoint.x, orbitingPoint.y);
            float cx = pivotPoint.x;
            float cy = pivotPoint.y;

            float s = -Mathf.Sin(angle * Mathf.Deg2Rad);
            float c = Mathf.Cos(angle * Mathf.Deg2Rad);

            // translate Vector2 back to origin:
            orbitingPoint.x -= cx;
            orbitingPoint.y -= cy;

            // rotate Vector2
            float xnew = orbitingPoint.x * c - orbitingPoint.y * s;
            float ynew = orbitingPoint.x * s + orbitingPoint.y * c;

            // translate Vector2 back:
            orbitingPoint.x = xnew + cx;
            orbitingPoint.y = ynew + cy;
            return orbitingPoint;
        }


        public override Collider[] GetHits(AbilityContext abilityContext)
        {
            RaycastHit[] boundingBoxHitEntities;
            List<RaycastHit> refinedHitEntities = new List<RaycastHit>();

            float relativesRotation = abilityContext.Get(RelativeTo, TransformVectorTypes.Rotation).y; //y coorinate is rotation in 3D space
            Vector3 relativesPosition3D = abilityContext.Get(RelativeTo, TransformVectorTypes.Position);
            Vector2 relativesPosition = new Vector2(relativesPosition3D.x, relativesPosition3D.z);
            Vector2 rotatePointAroundPoint2D = RotatePointAroundPoint(relativesPosition, relativesRotation, relativesPosition + boxDisplacement);
            Vector3 rotatePointAroundPoint3D = new Vector3(rotatePointAroundPoint2D.x, 0, rotatePointAroundPoint2D.y);

            Vector2[] pointsRotatedAndPositioned = RotatePointsInDirectionThatPlayerIsLooking(pointPositionsAfterSetup, relativesPosition, relativesRotation);
            boundingBoxHitEntities = Physics.BoxCastAll(rotatePointAroundPoint3D, boxSize, new Vector3(0, relativesRotation, 0), Quaternion.Euler(Vector3.zero), 0);
            DebugHandler.BoxCast(rotatePointAroundPoint3D, boxSize, relativesRotation, Vector2.zero, 0);
            string hitEntities = "";
            foreach (RaycastHit hit in boundingBoxHitEntities)
            {
                if (ContainsPoint(pointsRotatedAndPositioned, hit.collider.transform.position))
                {
                    refinedHitEntities.Add(hit);
                    hitEntities += hit.collider.name;
                }
            }

            foreach (ICustomShapeBehaviour b in Behaviours)
                b.OnCastShape(pointsRotatedAndPositioned);


            Debug.Log(refinedHitEntities.Count + " entities hit with custom shape : " + hitEntities);

            return refinedHitEntities.Select((RaycastHit hit) => hit.collider).ToArray(); ;
        }

        Vector2[] RotatePointsInDirectionThatPlayerIsLooking(Vector2[] points, Vector2 playerPosition, float playerRotation)
        {
            Vector2[] newPoints = new Vector2[points.Length];

            for (int ii = 0; ii < points.Length; ii++)
            {
                newPoints[ii] = RotatePointAroundPoint(new Vector3(0, 0 ,0), playerRotation, points[ii]);
                newPoints[ii] = newPoints[ii] + playerPosition;
            }

            return newPoints;
        }

        public override AreaShape Clone()
        {
            return new CustomAreaShape(this);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public class Custom2DAreaShape : AreaShape2D
    {
        //Constructor Fields
        private Vector2[] points;
        public Vector2[] Points { get { return points; } set { ProcessPoints(value); } }
        public Vector2 Pivot;
        public float PivotRotation;
        public float PivotRotationAroundCenter;
        public Vector2 PivotPositionRelativeToCenter;
        
        public List<ICustomShapeBehaviour> Behaviours { get; set; }

        //Calculated values
        private Vector2[] pointPositionsAfterSetup; 
        private Vector2 boxSize;
        private Vector2 boxDisplacement;

        public Custom2DAreaShape(AbilityContext.AbilityContextTransforms relativeTo, float distanceFromRelative, Vector2[] points) : base(relativeTo, 0, distanceFromRelative)
        {
            Points = points;
            Behaviours = new List<ICustomShapeBehaviour>();
        }

        public Custom2DAreaShape(AbilityContext.AbilityContextTransforms relativeTo, float distanceFromRelative, Vector2[] points, params ICustomShapeBehaviour[] customShapeBehaviours) : base(relativeTo, 0, distanceFromRelative)
        {
            Points = points;
            Behaviours = customShapeBehaviours.ToList();
        }

        public Custom2DAreaShape(Custom2DAreaShape shape) : base(shape.RelativeTo, shape.RotationOffset, shape.DistanceFromRelative)
        {
            Behaviours = Utilities.CloneList(shape.Behaviours);
            Vector2[] points = new Vector2[shape.Points.Length];
            for (int ii = 0; ii < shape.Points.Length; ii++)
                points[ii] = new Vector2(shape.Points[ii].x, shape.Points[ii].y);

            Points = points;
        }

        public void ProcessPoints(Vector2[] points)
        {
            this.points = points;

            pointPositionsAfterSetup = RelatePointsToPosition(points, Pivot, PivotRotation, PivotRotationAroundCenter, PivotPositionRelativeToCenter);
            GetBoundingBox(pointPositionsAfterSetup, out Vector2 bottomLeftMostPoint, out Vector2 topRightMostPoint);
            boxSize = topRightMostPoint - bottomLeftMostPoint;
            boxDisplacement = (bottomLeftMostPoint + topRightMostPoint) / 2;
        }

        //Gets the greatest individual direction for up, down, left, right from each point. Vector[0] is width, Vector[1] is height
        public void GetBoundingBox(Vector2[] points, out Vector2 bottomLeftMostPoint, out Vector2 topRightMostPoint)
        {
            Vector2[] boundingBoxPoints = new Vector2[4];

            float xMin = float.MaxValue;
            float xMax = float.MinValue;
            float yMin = float.MaxValue;
            float yMax = float.MinValue;

            for (int ii = 0; ii < points.Length; ii++)
            {
                if (points[ii].x < xMin) xMin = points[ii].x;
                if (points[ii].x > xMax) xMax = points[ii].x;
                if (points[ii].y < yMin) yMin = points[ii].y;
                if (points[ii].y > yMax) yMax = points[ii].y;
            }

            boundingBoxPoints[0] = new Vector2(xMin, yMin);
            boundingBoxPoints[1] = new Vector2(xMin, yMax);
            boundingBoxPoints[2] = new Vector2(xMax, yMax);
            boundingBoxPoints[3] = new Vector2(xMax, yMin);

            bottomLeftMostPoint = boundingBoxPoints[0];
            topRightMostPoint = boundingBoxPoints[2];
        }

        public Vector2[] RelatePointsToPosition(Vector2[] points, Vector2 pointPivot, float pivotRotation, float pivotsRotationAroundCenter, Vector2 pivotPositionRelativeToCenter)
        {
            Vector2[] pointsRelativePositionToPivot = new Vector2[points.Length];
            Vector2[] pointsPositionRelativeToPlayer = new Vector2[points.Length];

            for (int ii = 0; ii < points.Length; ii++)
            {
                //Points position relative to Pivots
                pointsRelativePositionToPivot[ii] = points[ii] - pointPivot;

                //Apply rotation of where pivot is facing
                pointsRelativePositionToPivot[ii] = RotatePointAroundPoint(new Vector2(0, 0), pivotRotation, pointsRelativePositionToPivot[ii]);

                //Apply Pivots position relative to player
                pointsPositionRelativeToPlayer[ii] = pointsRelativePositionToPivot[ii] + pivotPositionRelativeToCenter;

                // Apply Pivots rotation relative to player
                pointsPositionRelativeToPlayer[ii] = RotatePointAroundPoint(new Vector2(0, 0), pivotsRotationAroundCenter, pointsPositionRelativeToPlayer[ii]);
            }

            return pointsPositionRelativeToPlayer;
        }

        public bool ContainsPoint(Vector2[] points, Vector2 p)
        {
            var j = points.Length - 1;
            var inside = false;
            for (int i = 0; i < points.Length; j = i++)
            {
                var pi = points[i];
                var pj = points[j];
                Debug.DrawLine(pi, pj, Color.magenta);
                if (((pi.y <= p.y && p.y < pj.y) || (pj.y <= p.y && p.y < pi.y)) &&
                    (p.x < (pj.x - pi.x) * (p.y - pi.y) / (pj.y - pi.y) + pi.x))
                    inside = !inside;
            }
            return inside;
        }

        Vector2 RotatePointAroundPoint(Vector2 pivotPoint, float angle, Vector2 orbitingPoint)
        {
            Vector2 orbitedPoint = new Vector2(orbitingPoint.x, orbitingPoint.y);
            float cx = pivotPoint.x;
            float cy = pivotPoint.y;

            float s = Mathf.Sin(angle * Mathf.Deg2Rad);
            float c = Mathf.Cos(angle * Mathf.Deg2Rad);

            // translate Vector2 back to origin:
            orbitingPoint.x -= cx;
            orbitingPoint.y -= cy;

            // rotate Vector2
            float xnew = orbitingPoint.x * c - orbitingPoint.y * s;
            float ynew = orbitingPoint.x * s + orbitingPoint.y * c;

            // translate Vector2 back:
            orbitingPoint.x = xnew + cx;
            orbitingPoint.y = ynew + cy;
            return orbitingPoint;
        }


        public override RaycastHit2D[] GetHits(AbilityContext abilityContext)
        {
            RaycastHit2D[] boundingBoxHitEntities;
            List<RaycastHit2D> refinedHitEntities = new List<RaycastHit2D>();

            float relativesRotation = abilityContext.Get(RelativeTo, TransformVectorTypes.Rotation).z; //Z coorinate is rotation in 2D space
            Vector2 relativesPosition = (Vector2)abilityContext.Get(RelativeTo, TransformVectorTypes.Position);

            Vector2[] pointsRotatedAndPositioned = RotatePointsInDirectionThatPlayerIsLooking(pointPositionsAfterSetup, relativesPosition, relativesRotation);
            boundingBoxHitEntities = Physics2D.BoxCastAll(RotatePointAroundPoint(relativesPosition, relativesRotation, relativesPosition + boxDisplacement), boxSize, relativesRotation, Vector2.zero, 0);
            DebugHandler.BoxCast2D(RotatePointAroundPoint(relativesPosition, relativesRotation, relativesPosition + boxDisplacement), boxSize, relativesRotation, Vector2.zero, 0);
            string hitEntities = "";
            foreach (RaycastHit2D hit in boundingBoxHitEntities)
            {
                if (ContainsPoint(pointsRotatedAndPositioned, hit.collider.transform.position))
                {
                    refinedHitEntities.Add(hit);
                    hitEntities += hit.collider.name;
                }
            }

            foreach (ICustomShapeBehaviour b in Behaviours)
                b.OnCastShape(pointsRotatedAndPositioned);

            
            Debug.Log(refinedHitEntities.Count + " entities hit with custom shape : " + hitEntities);

            return refinedHitEntities.ToArray();
        }

        Vector2[] RotatePointsInDirectionThatPlayerIsLooking(Vector2[] points, Vector2 playerPosition, float playerRotation)
        {
            Vector2[] newPoints = new Vector2[points.Length];

            for (int ii = 0; ii < points.Length; ii++)
            {
                newPoints[ii] = RotatePointAroundPoint(new Vector2(0, 0), playerRotation, points[ii]);
                newPoints[ii] = newPoints[ii] + playerPosition;
            }

            return newPoints;
        }

        public override AreaShape2D Clone()
        {
            return new Custom2DAreaShape(this);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }        
    }

    public interface ICustomShapeBehaviour : ICloneable
    {
        void OnCastShape(Vector2[] points);        
    }

    public class DrawLinesCustomShapebehaviour : ICustomShapeBehaviour
    {
        public float Duration;
        public float Width;
        public Color Color;

        public DrawLinesCustomShapebehaviour(float duration, float width, Color color)
        {
            Duration = duration;
            Width = width;
            Color = color;
        }

        public DrawLinesCustomShapebehaviour Clone()
        {
            return new DrawLinesCustomShapebehaviour(Duration, Width, new Color(Color.r, Color.g, Color.b, Color.a));
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public void OnCastShape(Vector2[] points)
        {
            LineRenderer lineRenderer = new GameObject("LineRenderer").AddComponent<LineRenderer>();
            lineRenderer.material = Constants.UI.SpritesDefault;
            lineRenderer.startColor = this.Color;
            lineRenderer.endColor = this.Color;
            lineRenderer.startWidth = Width;
            lineRenderer.endWidth = Width;

            Vector3[] closedLoopOfPoints = new Vector3[points.Length + 1]; //Extra slot to close loop
            for (int ii=0; ii<closedLoopOfPoints.Length-1; ii++)
                closedLoopOfPoints[ii] = new Vector3(points[ii].x, 0, points[ii].y);

            closedLoopOfPoints[closedLoopOfPoints.Length-1] = new Vector3(points[0].x, 0, points[0].y);//closes the loop back to start

            lineRenderer.positionCount = closedLoopOfPoints.Length;
            lineRenderer.SetPositions(closedLoopOfPoints);
            GameObject.Destroy(lineRenderer.gameObject, Duration);
        }
    }
}