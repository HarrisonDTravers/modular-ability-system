﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class RaycastReference : MonoBehaviour
    {
        public Raycast Raycast { get; set; }
    }
}