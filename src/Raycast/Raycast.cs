﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class Raycast : ICloneable
    {
        public DeliveryRaycastBehaviour DeliveryRaycastBehaviour { get; set; }
        public List<EffectPackage> EffectPackages { get; set; }
        public List<IRaycastBehaviour> RaycastBehaviours { get; set; }
        public AbilityContext AbilityContext { get; set; }
        public RaycastReference RaycastReference { get; set; }
        public UnitMatrix UnitMatrix { get; set; }
        public int MaxDistance { get; set; }
        public int MaxTargets { get; set; }
        public Vector3 RaycastEnd { get; set; }

        public Raycast(List<EffectPackage> effectPackages, List<IRaycastBehaviour> raycastBehaviours, UnitMatrix unitMatrix, int maxDistance, int maxTargets)
        {
            this.EffectPackages = effectPackages;
            this.UnitMatrix = unitMatrix;
            this.RaycastBehaviours = raycastBehaviours;
            this.MaxDistance = maxDistance;
            this.MaxTargets = maxTargets;
        }

        public Raycast Instantiate(AbilityContext abilityContext, DeliveryRaycastBehaviour DeliveryRaycastBehaviour)
        {
            Raycast raycast = new Raycast(Utilities.CloneList(EffectPackages), Utilities.CloneList(RaycastBehaviours), UnitMatrix.Clone(), MaxDistance, MaxTargets);
            raycast.RaycastReference = new GameObject("Raycast").AddComponent<RaycastReference>();
            raycast.RaycastReference.Raycast = raycast;
            raycast.DeliveryRaycastBehaviour = DeliveryRaycastBehaviour;
            raycast.AbilityContext = abilityContext;
            raycast.RunBehaviours();
            GameObject.Destroy(raycast.RaycastReference.gameObject);

            return raycast;
        }

        public void RunBehaviours()
        {
            Dictionary<EntityAdapter, TransformMimic> hitEntities;
            RaycastEnd = DeliveryRaycastBehaviour.Deliver(this, out hitEntities);

            foreach (IRaycastBehaviour b in RaycastBehaviours)
            {
                b.Cast(this);
            }

            foreach (KeyValuePair<EntityAdapter, TransformMimic> receiver in hitEntities)
            {
                foreach (EffectPackage eP in EffectPackages)
                {
                    eP.Apply(new EffectContext(AbilityContext, receiver.Key, new TransformMimic(AbilityContext.Vessel.transform.position, AbilityContext.Vessel.transform.up, AbilityContext.Vessel.transform.rotation.eulerAngles), receiver.Value));
                }
            }
        }

        ICloneable ICloneable.IClone()
        {
            return this.Clone();
        }

        public Raycast Clone()
        {
            return new Raycast(Utilities.CloneList(EffectPackages), Utilities.CloneList(RaycastBehaviours), UnitMatrix.Clone(), MaxDistance, MaxTargets);
        }
    }
}