﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MAS
{
    public abstract class DeliveryRaycastBehaviour : ICloneable
    {
        public Vector3 Origin { get; set; }

        public DeliveryRaycastBehaviour(Vector3 origin)
        {
            Origin = origin;
        }

        public abstract Vector3 Deliver(Raycast raycast, out Dictionary<EntityAdapter, TransformMimic> hitEntities);
        public abstract ICloneable IClone();
    }

    public class RaycastInDirectionDeliveryRaycastBehaviour : DeliveryRaycastBehaviour
    {
        public Vector3 Direction { get; set; }

        public RaycastInDirectionDeliveryRaycastBehaviour(Vector3 origin, Vector3 direction) : base(origin)
        {
            this.Direction = direction;
        }

        public override ICloneable IClone()
        {
            return new RaycastInDirectionDeliveryRaycastBehaviour(Origin, Direction);
        }

        public override Vector3 Deliver(Raycast raycast, out Dictionary<EntityAdapter, TransformMimic> hitEntities)
        {
            return RaycastInDirection(raycast, out hitEntities);
        }

        public Vector3 RaycastInDirection(Raycast raycast, out Dictionary<EntityAdapter, TransformMimic> hitEntitiesOut)
        {
            Vector2 originPosition = Origin;
            RaycastHit2D[] raycastHits = Physics2D.RaycastAll(originPosition, Direction, raycast.MaxDistance);

#if (inDevelopment)
        if (DebugHandler.VerboseTargetBehaviours==true)
            Debug.Log(raycastHits.Length + " objects hit");
#endif

            Dictionary<EntityAdapter, TransformMimic> targetEntityCandidates = new Dictionary<EntityAdapter, TransformMimic>(); //Entities in the line of th raycast, but may not be targetable. (Ally in line of enemy targetted ability)

            foreach (RaycastHit2D raycastHit in raycastHits)
            {
                EntityAdapter hitEntity = raycastHit.collider.GetComponent<EntityAdapter>();
                if (hitEntity != null)
                    targetEntityCandidates.Add(hitEntity, new TransformMimic(raycastHit.point, raycastHit.normal, raycastHit.normal)); //donno what to do here
            }

#if (inDevelopment)
        if (DebugHandler.VerboseTargetBehaviours==true)
            Debug.Log(hitEntities.Count + " entities hit");
#endif

            hitEntitiesOut = new Dictionary<EntityAdapter, TransformMimic>();

            foreach (KeyValuePair<EntityAdapter, TransformMimic> hitEntityCandidate in targetEntityCandidates)
            {
                if (raycast.UnitMatrix.CanTarget(raycast.AbilityContext.Sender, hitEntityCandidate.Key))
                {
                    hitEntitiesOut.Add(hitEntityCandidate.Key, hitEntityCandidate.Value);
                }
            }

            if (hitEntitiesOut.Count > raycast.MaxTargets)
            {
                for (int ii = hitEntitiesOut.Count; ii > raycast.MaxTargets; ii--)
                    hitEntitiesOut.Remove(hitEntitiesOut.Keys.ElementAt(hitEntitiesOut.Count - 1));
            }

            Vector3 rayEnd;
            if (hitEntitiesOut.Count > 0 && hitEntitiesOut.Count >= raycast.MaxTargets) //If the max number of targets are hit then the last entity hit is where the ray ends. otherwise it's at the end of the ray's max length.
            {
                rayEnd = hitEntitiesOut.Values.ElementAt(hitEntitiesOut.Count - 1).Position;
            }
            else
            {
                rayEnd = Origin + (Direction * raycast.MaxDistance);
            }

            return rayEnd;
        }
    }

    public class RaycastTowardTargetRaycastBehaviour : DeliveryRaycastBehaviour
    {
        public EntityAdapter Target { get; set; }

        //TargetObserverBehaviours, destroy game object when target is destroyed. (observs entity.die())

        public RaycastTowardTargetRaycastBehaviour(Vector3 origin, EntityAdapter target) : base(origin)
        {
            Target = target;
        }

        public override ICloneable IClone()
        {
            return new RaycastTowardTargetRaycastBehaviour(Origin, Target);
        }

        public override Vector3 Deliver(Raycast raycast, out Dictionary<EntityAdapter, TransformMimic> hitEntities)
        {
            return RaycastTowardsTarget(raycast, out hitEntities);
        }

        public Vector3 RaycastTowardsTarget(Raycast raycast, out Dictionary<EntityAdapter, TransformMimic> hitEntitiesOut)
        {
            Dictionary<EntityAdapter, TransformMimic> entitiesHit = new Dictionary<EntityAdapter, TransformMimic>();
            Vector2 originPosition = Origin;
            Vector2 targetPosition = Target.gameObject.transform.position;
            Vector2 direction = (targetPosition - originPosition).normalized;
            RaycastHit2D[] raycastHits = Physics2D.RaycastAll(originPosition, direction, raycast.MaxDistance);

#if inDevelopment
        if (DebugHandler.VerboseTargetBehaviours==true)
            Debug.Log(raycastHits.Length + " objects hit");
#endif

            Dictionary<EntityAdapter, TransformMimic> hitEntityCandidates = new Dictionary<EntityAdapter, TransformMimic>(); //Entities in the line of th raycast, but may not be targetable. (Ally in line of enemy targetted ability)

            foreach (RaycastHit2D raycastHit in raycastHits)
            {
                EntityAdapter hitEntity = raycastHit.collider.GetComponent<EntityAdapter>();
                if (hitEntity != null)
                    hitEntityCandidates.Add(hitEntity, new TransformMimic(raycastHit.point, raycastHit.normal ,raycastHit.normal)); //dono what to do here
            }

#if inDevelopment
        if (DebugHandler.VerboseTargetBehaviours==true)
            Debug.Log(hitEntities.Count + " entities hit");
#endif

            Dictionary<EntityAdapter, TransformMimic> hitEntities = new Dictionary<EntityAdapter, TransformMimic>();

            foreach (KeyValuePair<EntityAdapter, TransformMimic> hitEntityCandidate in hitEntityCandidates)
            {
                if (raycast.UnitMatrix.CanTarget(raycast.AbilityContext.Sender, hitEntityCandidate.Key))
                {
                    hitEntities.Add(hitEntityCandidate.Key, hitEntityCandidate.Value);
                }
            }

            hitEntitiesOut = hitEntities; //The Linq function below requires the hitEntities list to not be a ref/out

            return raycastHits.First(x => x.collider.GetInstanceID() == hitEntities.ElementAt(hitEntities.Count - 1).Key.gameObject.GetInstanceID()).point;
        }
    }
}