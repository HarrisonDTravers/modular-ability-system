﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface IRaycastHitBehaviour : ICloneable
    {
        void OnHit(Raycast raycast, MES.Entity hitEntity);

    }
}
