﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface IRaycastBehaviour : ICloneable
    {
        void Cast(Raycast raycast);
    }

    public class LineRendererRaycastHitBehaviour : IRaycastBehaviour
    {
        //Replace all these with LineRendererBehaviours
        public Color Color { get; set; }
        public float Width { get; set; }
        public float LifeTime { get; set; }

        public LineRendererRaycastHitBehaviour(Color color, float width, float lifeTime)
        {
            Color = color;
            Width = width;
            LifeTime = lifeTime;
        }

        public void Cast(Raycast raycast)
        {
            LineRenderer lineRenderer = new GameObject("LineRenderer").AddComponent<LineRenderer>();
            lineRenderer.material = Beastlings.Constants.UI.SpritesDefault;
            lineRenderer.startColor = this.Color;
            lineRenderer.endColor = this.Color;
            lineRenderer.startWidth = Width;
            lineRenderer.endWidth = Width;
            lineRenderer.SetPositions(new Vector3[] { raycast.DeliveryRaycastBehaviour.Origin, raycast.RaycastEnd });

            MonobehaviourReference.Instance.StartCoroutine(DestroyAfterLifetime(lineRenderer));
        }

        public IEnumerator DestroyAfterLifetime(LineRenderer lineRenderer)
        {
            bool lifeEnded = false;
            float timePassed = 0;
            while (!lifeEnded)
            {
                if (timePassed >= LifeTime) lifeEnded = true;
                timePassed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            GameObject.Destroy(lineRenderer.gameObject);
        }

        public ICloneable IClone()
        {
            return new LineRendererRaycastHitBehaviour(new Color(Color.r, Color.g, Color.b, Color.a), Width, LifeTime);
        }

    }
}