﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public abstract class AbilityConfiguration
    {
        public AbilityCourse AbilityCourse { get; set; }

        public AbilityConfiguration(AbilityCourse abilityCourse)
        {
            AbilityCourse = abilityCourse;
        }

        public abstract void Configure(AbilityContext abilityContext, Ability ability, int abilityIndex);

        public abstract AbilityConfiguration Clone();
    }

    public class ActiveAbilityConfiguration : AbilityConfiguration
    {
        public ActiveAbilityConfiguration(AbilityCourse abilityCourse) : base(abilityCourse) { }

        public override void Configure(AbilityContext abilityContext, Ability ability, int abilityIndex)
        {
            abilityContext.Sender.AbilityBehaviour.CastEvents[abilityIndex].OnAbilityCast += () => ability.Cast(abilityContext);
        }

        public override AbilityConfiguration Clone()
        {
            return new ActiveAbilityConfiguration(AbilityCourse.Clone());
        }
    }

    public class PassiveAbilityConfiguration : AbilityConfiguration
    {
        public PassiveAbilityConfiguration(AbilityCourse abilityCourse) : base(abilityCourse) { }

        public override void Configure(AbilityContext abilityContext, Ability ability, int abilityIndex)
        {
            AbilityCourse.RunCourse(abilityContext);
        }

        public override AbilityConfiguration Clone()
        {
            return new PassiveAbilityConfiguration(AbilityCourse.Clone());
        }
    }
}