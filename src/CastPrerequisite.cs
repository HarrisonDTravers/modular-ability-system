﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public abstract class CastBlocker : ICloneable
    {
        public Ability Ability { get; set; }
        protected float BlockerValue { get; set; } //A value between 0 and 1 to represent how long the blocker will last
        public event Constants.delVoidInputFloat OnBlockerStateUpdate;
        public bool IsOnBlockerStateUpdateNull() { return OnBlockerStateUpdate == null ? true : false; }

        public CastBlocker()
        {
        }

        public void Unblock()
        {
            Ability.RemoveCastBlocker(this);
        }

        public void Block(Ability ability)
        {
            Ability = ability;
            Ability.AddCastBlocker(this);
            Run();
        }

        protected void BlockerStateUpdated()
        {
            if (OnBlockerStateUpdate != null) OnBlockerStateUpdate.Invoke(BlockerValue);
        }

        protected abstract void Run();

        public abstract ICloneable IClone();
    }

    public class CooldownCastBlocker : CastBlocker
    {
        private bool Cancelled { get; set; }
        private float Cooldown { get; set; }
        public float GetCooldown() { return Cooldown; }
        private float RemainingCooldown { get; set; }
        public float GetRemainingCooldown { get; set; }

        public event Constants.delVoid OnCooldownStart;
        public event Constants.delVoid OnCooldownEnd;

        public CooldownCastBlocker(float cooldown)
        {
            Cooldown = cooldown;
        }

        protected override void Run()
        {
            MonobehaviourReference.Instance.StartCoroutine(CooldownRoutine());
        }

        public IEnumerator CooldownRoutine()
        {
            Cancelled = false;
            RemainingCooldown = Cooldown;
            if (OnCooldownStart != null) OnCooldownStart.Invoke();
            while (!Cancelled && RemainingCooldown > 0)
            {
                RemainingCooldown -= Time.fixedDeltaTime;
                BlockerValue = RemainingCooldown / Cooldown;
                try
                {
                    BlockerStateUpdated();
                }
                catch(MissingReferenceException)
                {
                    Cancelled = true;
                }
                yield return new WaitForFixedUpdate();
            }
            if(!Cancelled)
            {
                OnCooldownEnd?.Invoke();
                Unblock();
            }            
        }

        public CooldownCastBlocker Clone()
        {
            return new CooldownCastBlocker(Cooldown);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }
}