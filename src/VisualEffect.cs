using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class VisualEffect
    {
        public interface IVisualEffect : ICloneable
        {
            public void Run(AbilityContext abilityContext);
        }

        public class MorphAndAnimate : IVisualEffect
        {
            public Animator MorphPrefab { get; set; }

            public MorphAndAnimate(Animator morphPrefab)
            {
                MorphPrefab = morphPrefab;
            }

            public MorphAndAnimate(MorphAndAnimate original)
            {
                MorphPrefab = original.MorphPrefab;
            }

            public void Run(AbilityContext abilityContext)
            {

            }

            public ICloneable IClone()
            {
                return new MorphAndAnimate(this);
            }
        }
    }
}