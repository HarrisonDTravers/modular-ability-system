﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace MAS
{
    public interface ITargettedDeliveryBehaviour : ICloneable
    {
        void SendDelivery(AbilityContext abilityContext, EntityAdapter target);
        ITargettedDeliveryBehaviour Clone();
    }

    public interface IAimedDeliveryBehaviour : ICloneable
    {
        void SendDelivery(AbilityContext abilityContext, Vector3 direction);
        IAimedDeliveryBehaviour Clone();
    }

    public class InDirectionProjectileDeliveryBehaviour : ProjectileDeliveryBehaviour, IAimedDeliveryBehaviour
    {
        public InDirectionProjectileDeliveryBehaviour(float projectileSpeed, Projectile projectile) : base(projectileSpeed, projectile)
        {
        }

        public InDirectionProjectileDeliveryBehaviour(Constants.delT<float> projectileSpeed, Projectile projectile) : base(projectileSpeed, projectile)
        {
        }

        public InDirectionProjectileDeliveryBehaviour(float projectileSpeed, params IProjectileBehaviour[] projectileBehaviours) : base(projectileSpeed, projectileBehaviours)
        {
        }

        public void SendDelivery(AbilityContext abilityContext, Vector3 direction)
        {
            Projectile projectileInstance = Projectile.Instantiate(abilityContext, new TranslateInDirectionDeliveryProjectileBehaviour(direction, ProjectileSpeed));
        }

        public InDirectionProjectileDeliveryBehaviour Clone()
        {
            return new InDirectionProjectileDeliveryBehaviour(ProjectileSpeed, Projectile.Clone());
        }

        IAimedDeliveryBehaviour IAimedDeliveryBehaviour.Clone()
        {
            return Clone();
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public class FollowTargetProjectileDeliveryBehaviour : ProjectileDeliveryBehaviour, ITargettedDeliveryBehaviour
    {
        public FollowTargetProjectileDeliveryBehaviour(float projectileSpeed, Projectile projectile) : base(projectileSpeed, projectile)
        {
        }

        public FollowTargetProjectileDeliveryBehaviour(Constants.delT<float> projectileSpeed, Projectile projectile) : base(projectileSpeed, projectile)
        {
        }

        public FollowTargetProjectileDeliveryBehaviour(float projectileSpeed, params IProjectileBehaviour[] projectileBehaviours) : base(projectileSpeed, projectileBehaviours)
        {
        }

        public void SendDelivery(AbilityContext abilityContext, EntityAdapter target)
        {
            Projectile projectileInstance = Projectile.Instantiate(abilityContext, new TranslateTowardsTargetProjectileBehaviour(target, ProjectileSpeed));
        }

        public FollowTargetProjectileDeliveryBehaviour Clone()
        {
            return new FollowTargetProjectileDeliveryBehaviour(ProjectileSpeed, Projectile.Clone());
        }

        ITargettedDeliveryBehaviour ITargettedDeliveryBehaviour.Clone()
        {
            return Clone();
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public class TowardsTargetProjectileDeliveryBehaviour : ProjectileDeliveryBehaviour, ITargettedDeliveryBehaviour
    {
        public TowardsTargetProjectileDeliveryBehaviour(float projectileSpeed, Projectile projectile) : base(projectileSpeed, projectile)
        {
        }

        public TowardsTargetProjectileDeliveryBehaviour(Constants.delT<float> projectileSpeed, Projectile projectile) : base(projectileSpeed, projectile)
        {
        }

        public TowardsTargetProjectileDeliveryBehaviour(float projectileSpeed, params IProjectileBehaviour[] projectileBehaviours) : base(projectileSpeed, projectileBehaviours)
        {
        }

        public void SendDelivery(AbilityContext abilityContext, EntityAdapter target)
        {
            Vector3 directionFromSenderToTarget = target.transform.position - abilityContext.Sender.transform.position;
            Projectile projectileInstance = Projectile.Instantiate(abilityContext, new TranslateInDirectionDeliveryProjectileBehaviour(directionFromSenderToTarget, ProjectileSpeed));
        }

        public TowardsTargetProjectileDeliveryBehaviour Clone()
        {
            return new TowardsTargetProjectileDeliveryBehaviour(ProjectileSpeed, Projectile.Clone());
        }

        ITargettedDeliveryBehaviour ITargettedDeliveryBehaviour.Clone()
        {
            return Clone();
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public abstract class ProjectileDeliveryBehaviour : ICloneable
    {
        public Projectile Projectile { get; set; }
        public bool AddedToList { get; set; }
        public Constants.delT<float> ProjectileSpeed { get; set; }

        public ProjectileDeliveryBehaviour(float projectileSpeed, Projectile projectile)
        {
            Projectile = projectile;
            ProjectileSpeed = () => projectileSpeed;
        }

        public ProjectileDeliveryBehaviour(Constants.delT<float> projectileSpeed, Projectile projectile)
        {
            Projectile = projectile;
            ProjectileSpeed = projectileSpeed;
        }

        public ProjectileDeliveryBehaviour(float projectileSpeed, params IProjectileBehaviour[] projectileBehaviours)
        {
            Projectile = new Projectile(projectileBehaviours.ToList());
            ProjectileSpeed = () => projectileSpeed;
        }

        public abstract ICloneable IClone();
    }

    public class RaycastDeliveryBehaviour : IAimedDeliveryBehaviour, ITargettedDeliveryBehaviour
    {
        public Raycast Raycast { get; set; }
        public EntityAdapter Entity { get; set; }
        public DeliveryRaycastBehaviour DeliveryRaycastBehaviour { get; set; } //The IProjectileBehaviour supplied by the abilities type, only one is supplied by the abilitytype
        public delegate void DelOnRayCastHit(RaycastHit2D hit);

        public RaycastDeliveryBehaviour(Raycast raycast)
        {
            Raycast = raycast;
        }

        public void SendDelivery(AbilityContext abilityContext, EntityAdapter target)
        {
            Raycast raycastInstance = Raycast.Instantiate(abilityContext, new RaycastTowardTargetRaycastBehaviour(abilityContext.Vessel.gameObject.transform.position, target));
        }

        public void SendDelivery(AbilityContext abilityContext, Vector3 direction)
        {
            Raycast raycastInstance = Raycast.Instantiate(abilityContext, new RaycastInDirectionDeliveryRaycastBehaviour(abilityContext.Vessel.gameObject.transform.position, direction));
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        ITargettedDeliveryBehaviour ITargettedDeliveryBehaviour.Clone()
        {
            return Clone();
        }

        IAimedDeliveryBehaviour IAimedDeliveryBehaviour.Clone()
        {
            return Clone();
        }

        public RaycastDeliveryBehaviour Clone()
        {
            return new RaycastDeliveryBehaviour(Raycast.Clone());
        }
    }

    public class InstantDeliveryBehaviour : ITargettedDeliveryBehaviour, ICloneable
    {
        public EntityAdapter Target { get; set; }
        public List<EffectPackage> EffectPackages { get; set; }
        public List<IOnInstantDeliveryBehaviour> OnDeliveryBehaviours { get; set; }        

        public InstantDeliveryBehaviour(List<EffectPackage> effectPackages)
        {
            EffectPackages = effectPackages;
            OnDeliveryBehaviours = new List<IOnInstantDeliveryBehaviour>();
        }

        public InstantDeliveryBehaviour(params EffectPackage[] effectPackages)
        {
            EffectPackages = new List<EffectPackage>(effectPackages);
            OnDeliveryBehaviours = new List<IOnInstantDeliveryBehaviour>();
        }

        public InstantDeliveryBehaviour(List<EffectPackage> effectPackages, List<IOnInstantDeliveryBehaviour> onDeliveryBehaviours)
        {
            EffectPackages = effectPackages;
            OnDeliveryBehaviours = onDeliveryBehaviours;
        }

        public void SendDelivery(AbilityContext abilityContext, EntityAdapter target)
        {
            EffectContext eC = new EffectContext(abilityContext, target, new TransformMimic(abilityContext.Vessel.transform.position, abilityContext.Vessel.transform.forward, abilityContext.Vessel.transform.rotation.eulerAngles), new TransformMimic(target.transform.position, target.transform.up, target.transform.rotation.eulerAngles));
            foreach (EffectPackage e in EffectPackages)
                //cast location and collision will be senders transform at cast time and receivers transform as collision time
                e.Apply(eC);
            foreach (IOnInstantDeliveryBehaviour e in OnDeliveryBehaviours)
                e.Run(eC);
        }

        public void SetTarget(EntityAdapter origin, EntityAdapter target)
        {
            Target = target;
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        ITargettedDeliveryBehaviour ITargettedDeliveryBehaviour.Clone()
        {
            return Clone();
        }

        public InstantDeliveryBehaviour Clone()
        {
            return new InstantDeliveryBehaviour(Utilities.CloneList(EffectPackages), Utilities.CloneList<IOnInstantDeliveryBehaviour>(OnDeliveryBehaviours));
        }
    }    
}

