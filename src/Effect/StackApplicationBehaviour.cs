﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class StackApplicationBehaviour : IEffectApplicationBehaviour
    {
        public DurationEffectBehaviour DurationEffectBehaviour { get; set; }
        public List<DurationEffectStackBehaviour> StackBehaviours { get; set; }

        public StackApplicationBehaviour(DurationEffectBehaviour durationEffectBehaviour)
        {
            DurationEffectBehaviour = durationEffectBehaviour;
            StackBehaviours = new List<DurationEffectStackBehaviour>
        {
            new ResetDurationStackBehaviour(new SingleDigitStackRequirementBehaviour(1)),
            new IncrementCountDurationStackBehaviour(new SingleDigitStackRequirementBehaviour(0))
        };
        }

        public StackApplicationBehaviour(DurationEffectBehaviour durationEffectBehaviour, List<DurationEffectStackBehaviour> stackBehaviours)
        {
            DurationEffectBehaviour = durationEffectBehaviour;
            StackBehaviours = stackBehaviours;
        }

        public StackApplicationBehaviour(StackApplicationBehaviour original)
        {
            DurationEffectBehaviour = original.DurationEffectBehaviour.Clone(); 
            StackBehaviours = Utilities.CloneList(original.StackBehaviours);
        }

        public void Apply(EffectContext effectContext)
        {
            if (effectContext.Receiver.StackHandler.GetStackCount(DurationEffectBehaviour) < 1)
            {
                DurationEffectBehaviour.Apply(effectContext);
            }
            foreach (DurationEffectStackBehaviour stackBehaviour in StackBehaviours)
            {
                if (stackBehaviour.CanApply(effectContext, DurationEffectBehaviour))
                {
                    stackBehaviour.Apply(effectContext, DurationEffectBehaviour);
                }
            }
        }

        public void Cancel()
        {
            DurationEffectBehaviour.Cancel();
            StackBehaviours.ForEach(x => x.Cancel());
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        IEffectApplicationBehaviour IEffectApplicationBehaviour.Clone()
        {
            return Clone();
        }

        public StackApplicationBehaviour Clone()
        {
            return new StackApplicationBehaviour(this);
        }
    }
}