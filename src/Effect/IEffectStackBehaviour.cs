﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public abstract class DurationEffectStackBehaviour : ICloneable
    {
        public IStackRequirementBehaviour StackRequirementBehaviour { get; set; }

        public DurationEffectStackBehaviour(IStackRequirementBehaviour stackRequirementBehaviour)
        {
            StackRequirementBehaviour = stackRequirementBehaviour;
        }

        public abstract void Apply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour);

        public abstract bool CanApply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour);

        public abstract void Cancel();

        public abstract ICloneable IClone();
    }

    public class ResetDurationStackBehaviour : DurationEffectStackBehaviour
    {
        public ResetDurationStackBehaviour(IStackRequirementBehaviour stackRequirementBehaviour) : base(stackRequirementBehaviour) { }

        public override void Apply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            DurationEffectBehaviour preExistingStack = effectContext.Receiver.StackHandler.DurationEffects[durationEffectBehaviour.DurationEffectId];
            preExistingStack.RemainingDuration = preExistingStack.Duration;
        }

        public override bool CanApply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            return StackRequirementBehaviour.RequirementsMet(effectContext, durationEffectBehaviour);
        }

        public override void Cancel()
        {
            //Nothing to do there
        }

        public override ICloneable IClone()
        {
            return new ResetDurationStackBehaviour(StackRequirementBehaviour.Clone());
        }
    }

    public class ReplaceEffectStackBehaviour : DurationEffectStackBehaviour
    {
        public ReplaceEffectStackBehaviour(IStackRequirementBehaviour stackRequirementBehaviour) : base(stackRequirementBehaviour) { }

        public override void Apply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            DurationEffectBehaviour preExistingStack = effectContext.Receiver.StackHandler.DurationEffects[durationEffectBehaviour.DurationEffectId];
            preExistingStack.Cancel();
            durationEffectBehaviour.Apply(effectContext);
        }

        public override bool CanApply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            return StackRequirementBehaviour.RequirementsMet(effectContext, durationEffectBehaviour);
        }

        public override void Cancel()
        {
            //Nothing to do there
        }

        public override ICloneable IClone()
        {
            return new ResetDurationStackBehaviour(StackRequirementBehaviour.Clone());
        }
    }

    public class IncrementCountDurationStackBehaviour : DurationEffectStackBehaviour
    {
        public IncrementCountDurationStackBehaviour(IStackRequirementBehaviour stackRequirementBehaviour) : base(stackRequirementBehaviour) { }

        public override void Apply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            DurationEffectBehaviour preExistingStack = effectContext.Receiver.StackHandler.DurationEffects[durationEffectBehaviour.DurationEffectId];
            preExistingStack.StackCount++;
        }

        public override bool CanApply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            return StackRequirementBehaviour.RequirementsMet(effectContext, durationEffectBehaviour);
        }

        public override void Cancel()
        {
            //Nothing to do there
        }

        public override ICloneable IClone()
        {
            return new IncrementCountDurationStackBehaviour(StackRequirementBehaviour.Clone());
        }
    }

    public class ApplyEffectDurationStackBehaviour : DurationEffectStackBehaviour
    {
        public StackApplicationBehaviour StackApplicationBehaviour { get; set; }

        public ApplyEffectDurationStackBehaviour(StackApplicationBehaviour stackApplicationBehaviour, IStackRequirementBehaviour stackRequirementBehaviour) : base(stackRequirementBehaviour)
        {
            StackApplicationBehaviour = stackApplicationBehaviour;
        }

        public override void Apply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            StackApplicationBehaviour.Apply(effectContext);
        }

        public override bool CanApply(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            return StackRequirementBehaviour.RequirementsMet(effectContext, durationEffectBehaviour);
        }

        public override void Cancel()
        {
            StackApplicationBehaviour.Cancel();
        }

        public override ICloneable IClone()
        {
            return new ApplyEffectDurationStackBehaviour(StackApplicationBehaviour.Clone(), StackRequirementBehaviour.Clone());
        }
    }
}