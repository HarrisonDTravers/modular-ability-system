﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface IStackRequirementBehaviour : ICloneable
    {
        bool RequirementsMet(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour);
        IStackRequirementBehaviour Clone();
    }

    public class SingleDigitStackRequirementBehaviour : IStackRequirementBehaviour
    {
        public int RequiredStackCount { get; set; }

        public SingleDigitStackRequirementBehaviour(int requiredStackCount)
        {
            RequiredStackCount = requiredStackCount;
        }

        public bool RequirementsMet(EffectContext effectContext, DurationEffectBehaviour durationEffectBehaviour)
        {
            if (effectContext.Receiver.StackHandler.GetStackCount(durationEffectBehaviour) == RequiredStackCount)
                return true;
            else
                return false;
        }

        ICloneable ICloneable.IClone()
        {
            return Clone();
        }

        public IStackRequirementBehaviour Clone()
        {
            return new SingleDigitStackRequirementBehaviour(RequiredStackCount);
        }
    }
}