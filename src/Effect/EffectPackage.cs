﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MAS
{
    public class EffectPackage : ICloneable
    {
        public static int InstanceIdCount { get; set; }
        public int InstanceId { get; set; }
        public string Name { get; set; }

        public List<IEffectApplicationBehaviour> ApplicationBehaviours { get; set; }

        public EffectPackage(string name, List<IEffectApplicationBehaviour> applicationBehaviours)
        {
            InstanceId = InstanceIdCount++;
            Name = name;
            ApplicationBehaviours = applicationBehaviours;
        }

        public EffectPackage(string name, params IEffectApplicationBehaviour[] applicationBehaviours)
        {
            InstanceId = InstanceIdCount++;
            Name = name;
            ApplicationBehaviours = applicationBehaviours.ToList();
        }

        public void Apply(EffectContext effectContext)
        {
            foreach (IEffectApplicationBehaviour b in ApplicationBehaviours)
            {
                b.Clone().Apply(effectContext);
            }
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public EffectPackage Clone()
        {
            return new EffectPackage(Name, Utilities.CloneList(ApplicationBehaviours));
        }
    }
}