﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface IEffectApplicationBehaviour : ICloneable
    {
        void Apply(EffectContext effectContext);
        IEffectApplicationBehaviour Clone();
    }    

    public class ApplyEffectBehaviour : IEffectApplicationBehaviour
    {
        public Effect Effect { get; set; }
        public List<IOnEffectApplication> OnEffectApply { get; set; }
        public List<IOnEffectApplication> OnEffectUnapply { get; set; }

        public ApplyEffectBehaviour(Effect effect)
        {
            Effect = effect;
            OnEffectApply = new List<IOnEffectApplication>();
            OnEffectUnapply = new List<IOnEffectApplication>();
        }

        public ApplyEffectBehaviour(Effect effect, List<IOnEffectApplication> onEffectApply, List<IOnEffectApplication> onEffectUnapply)
        {
            Effect = effect;
            OnEffectApply = onEffectApply;
            OnEffectUnapply = onEffectUnapply;
        }

        public void Apply(EffectContext effectContext)
        {
            Effect.ApplyEffect(effectContext);

            foreach(IOnEffectApplication eA in OnEffectApply)
            {
                eA.EffectApplied(effectContext);
            }
        }

        public void Unapply(EffectContext effectContext)
        {
            Effect.RemoveEffect();

            foreach (IOnEffectApplication eU in OnEffectUnapply)
            {
                eU.EffectApplied(effectContext);
            }
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public IEffectApplicationBehaviour Clone()
        {
            return new ApplyEffectBehaviour(Effect.Clone());
        }
    }

}