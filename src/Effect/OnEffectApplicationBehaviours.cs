﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface IOnEffectApplication
    {
        void EffectApplied(EffectContext effectContext);
    }

    public static class OnEffectApplication
    {
        public class CastAbilityOnEffectApplication : IOnEffectApplication, ICloneable
        {
            public Ability Ability { get; set; }

            public CastAbilityOnEffectApplication(Ability ability)
            {
                this.Ability = ability;
            }

            public void EffectApplied(EffectContext effectContext)
            {
                Ability.Cast(effectContext.GetAbilityContext());
            }

            public CastAbilityOnEffectApplication Clone()
            {
                return new CastAbilityOnEffectApplication(Ability.Clone());
            }

            public ICloneable IClone()
            {
                return Clone();
            }
        }
    }

    
}
