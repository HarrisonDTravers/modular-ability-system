﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static MAS.DisplacementMethods;
using static MAS.RelativeTo;

namespace MAS
{
    //Base ABSTRACT class for ALL time related effect behaviours. NOT CONCRETE
    public abstract class DurationEffectBehaviour : ICloneable
    {
        public static int DurationEffectCount { get; set; }
        public static int InstanceCount { get; set; }

        public int DurationEffectId { get; set; }
        public int InstanceId { get; set; }

        public int StackCount;

        public List<StackApplicationBehaviour> StackBehaviours { get; set; }
        public List<ApplyEffectBehaviour> ApplyEffectBehaviours { get; set; }
        public List<DurationEffectCompleted> DurationEffectCompletedBehaviours { get; set; }
        public List<DuringDurationEffect> DuringDurationEffects { get; set; }
        public float Duration { get; set; }
        public float RemainingDuration { get; set; } //Represents time remaining on effect
        public bool RoutineCancelled { get; set; }

        public DurationEffectBehaviour()
        {
            DurationEffectId = DurationEffectCount++;
            StackBehaviours = new List<StackApplicationBehaviour>();
            DurationEffectCompletedBehaviours = new List<DurationEffectCompleted>();
            DuringDurationEffects = new List<DuringDurationEffect>();
        }

        public DurationEffectBehaviour(float duration, List<ApplyEffectBehaviour> applyEffectBehaviours)
        {
            DurationEffectId = DurationEffectCount++;
            Duration = duration;
            ApplyEffectBehaviours = applyEffectBehaviours;
            StackBehaviours = new List<StackApplicationBehaviour>();
            DurationEffectCompletedBehaviours = new List<DurationEffectCompleted>();
            DuringDurationEffects = new List<DuringDurationEffect>();
        }

        public DurationEffectBehaviour(float duration, params ApplyEffectBehaviour[] applyEffectBehaviours)
        {
            DurationEffectId = DurationEffectCount++;
            Duration = duration;
            ApplyEffectBehaviours = new List<ApplyEffectBehaviour>(applyEffectBehaviours);
            StackBehaviours = new List<StackApplicationBehaviour>();
            DurationEffectCompletedBehaviours = new List<DurationEffectCompleted>();
            DuringDurationEffects = new List<DuringDurationEffect>();
        }

        public DurationEffectBehaviour(float duration, List<DurationEffectCompleted> durationEffectCompleted, List<ApplyEffectBehaviour> applyEffectBehaviours)
        {
            DurationEffectId = DurationEffectCount++;
            Duration = duration;
            ApplyEffectBehaviours = new List<ApplyEffectBehaviour>(applyEffectBehaviours);
            DurationEffectCompletedBehaviours = durationEffectCompleted;
            DuringDurationEffects = new List<DuringDurationEffect>();
            StackBehaviours = new List<StackApplicationBehaviour>();
        }

        public DurationEffectBehaviour(float duration, List<DurationEffectCompleted> durationEffectCompleted, List<DuringDurationEffect> duringDurationEffects, List<ApplyEffectBehaviour> applyEffectBehaviours)
        {
            DurationEffectId = DurationEffectCount++;
            Duration = duration;
            ApplyEffectBehaviours = new List<ApplyEffectBehaviour>(applyEffectBehaviours);
            DurationEffectCompletedBehaviours = durationEffectCompleted;
            DuringDurationEffects = duringDurationEffects;
            StackBehaviours = new List<StackApplicationBehaviour>();
        }

        public DurationEffectBehaviour(float duration, List<Effect> effects)
        {
            DurationEffectId = DurationEffectCount++;
            Duration = duration;
            ApplyEffectBehaviours = new List<ApplyEffectBehaviour>();
            StackBehaviours = new List<StackApplicationBehaviour>();
            DurationEffectCompletedBehaviours = new List<DurationEffectCompleted>();
            DuringDurationEffects = new List<DuringDurationEffect>();

            foreach (Effect effect in effects)
                ApplyEffectBehaviours.Add(new ApplyEffectBehaviour(effect));
        }

        public DurationEffectBehaviour(int durationEffectId, float duration, List<ApplyEffectBehaviour> applyEffectBehaviours)
        {
            DurationEffectId = durationEffectId;
            Duration = duration;
            ApplyEffectBehaviours = applyEffectBehaviours;
            StackBehaviours = new List<StackApplicationBehaviour>();
            DurationEffectCompletedBehaviours = new List<DurationEffectCompleted>();
            DuringDurationEffects = new List<DuringDurationEffect>();
        }

        public DurationEffectBehaviour(DurationEffectBehaviour durationEffectBehaviour)
        {
            InstanceId = InstanceCount++;
            Duration = durationEffectBehaviour.Duration;
            DurationEffectId = durationEffectBehaviour.DurationEffectId;
            StackBehaviours = Utilities.CloneList(durationEffectBehaviour.StackBehaviours);
            ApplyEffectBehaviours = Utilities.CloneList(durationEffectBehaviour.ApplyEffectBehaviours);
            DurationEffectCompletedBehaviours = Utilities.CloneList(durationEffectBehaviour.DurationEffectCompletedBehaviours);
            DuringDurationEffects = Utilities.CloneList(durationEffectBehaviour.DuringDurationEffects);
        }

        public abstract void Apply(EffectContext effectContext);

        public abstract void Cancel();
        protected abstract void CancelRoutine();
        protected abstract IEnumerator TemporalRoutine(EffectContext effectContext);
        public abstract ICloneable IClone();
        public abstract DurationEffectBehaviour Clone();
    }

    public class TemporaryEffectBehaviour : DurationEffectBehaviour
    {
        public TemporaryEffectBehaviour(float duration, params ApplyEffectBehaviour[] applyEffectBehaviours) : base(duration, applyEffectBehaviours)
        {
        }

        public TemporaryEffectBehaviour(float duration, List<ApplyEffectBehaviour> applyEffectBehaviours) : base(duration, applyEffectBehaviours)
        {
        }

        public TemporaryEffectBehaviour(int durationEffectId, float duration, List<ApplyEffectBehaviour> applyEffectBehaviours) : base(durationEffectId, duration, applyEffectBehaviours)
        {
        }

        public TemporaryEffectBehaviour(float duration, params Effect[] effects) : base(duration, effects.ToList()) 
        {
        }

        public TemporaryEffectBehaviour(TemporaryEffectBehaviour temporaryEffectBehaviour) : base(temporaryEffectBehaviour)
        {
            Duration = temporaryEffectBehaviour.Duration;
            StackBehaviours = Utilities.CloneList(StackBehaviours);
            ApplyEffectBehaviours = Utilities.CloneList(ApplyEffectBehaviours);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override DurationEffectBehaviour Clone()
        {
            return new TemporaryEffectBehaviour(DurationEffectId, Duration, Utilities.CloneList(ApplyEffectBehaviours));
        }

        public override void Apply(EffectContext effectContext)
        {
            RunRoutine(effectContext);
        }

        public void RunRoutine(EffectContext effectContext)
        {
            MonobehaviourReference.Instance.StartCoroutine(TemporalRoutine(effectContext));
        }

        public override void Cancel()
        {
            CancelRoutine();
        }

        protected override IEnumerator TemporalRoutine(EffectContext effectContext)
        {
            RemainingDuration = Duration;
            effectContext.Receiver.StackHandler.AddStack(this);
            ApplyEffectBehaviours.ForEach(x => x.Apply(effectContext));

            float timePassed = 0;
            float timeStarted = Time.fixedTime;
            float timeToEnd = Time.fixedTime + Duration;
            Debug.Log($"Temporary - Start: {timeStarted}, End: timeToEnd {timeToEnd}");
            while (!RoutineCancelled && timeToEnd > Time.fixedTime)
            {
                timePassed = (Time.fixedTime - timeStarted) / Duration;
                yield return new WaitForFixedUpdate();
            }
            Debug.Log($"Temporary - Actual End: {Time.fixedTime}");
            ApplyEffectBehaviours.ForEach(x => x.Unapply(effectContext));
            effectContext.Receiver.StackHandler.RemoveStack(this);
            RoutineCancelled = false;
            DurationEffectCompletedBehaviours.ForEach(x => x.Run(effectContext));
        }

        protected override void CancelRoutine()
        {
            RoutineCancelled = false;
        }
    }

    //Applys an EffectApplicationBehaviour multiple times a second for a predefined amount of seconds
    public class ReoccuringApplicationBehaviour : DurationEffectBehaviour
    {
        public float DelayBetweenOccurances { get; set; }

        public List<StackApplicationBehaviour> StackApplicationBehaviours { get; set; }

        public ReoccuringApplicationBehaviour(float duration, float delayBetweenOccurances, List<StackApplicationBehaviour> stackApplicationBehaviours, List<ApplyEffectBehaviour> applyEffectBehaviours) : base(duration, applyEffectBehaviours)
        {
            DelayBetweenOccurances = delayBetweenOccurances;
            StackApplicationBehaviours = stackApplicationBehaviours;
        }

        public ReoccuringApplicationBehaviour(int durationEffectId, float duration, float delayBetweenOccurances, List<StackApplicationBehaviour> stackApplicationBehaviours, List<ApplyEffectBehaviour> applyEffectBehaviours) : base(durationEffectId, duration, applyEffectBehaviours)
        {
            DelayBetweenOccurances = delayBetweenOccurances;
            StackApplicationBehaviours = stackApplicationBehaviours;
        }

        public ReoccuringApplicationBehaviour(ReoccuringApplicationBehaviour original) : base(original)
        {
            DelayBetweenOccurances = original.DelayBetweenOccurances;
            StackApplicationBehaviours = original.StackApplicationBehaviours;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override DurationEffectBehaviour Clone()
        {
            return new ReoccuringApplicationBehaviour(DurationEffectId, Duration, DelayBetweenOccurances, Utilities.CloneList(StackApplicationBehaviours), Utilities.CloneList(ApplyEffectBehaviours));
        }

        public override void Cancel()
        {
            CancelRoutine();
            StackApplicationBehaviours.ForEach(x => x.Cancel());
        }

        protected override IEnumerator TemporalRoutine(EffectContext effectContext)
        {
            RoutineCancelled = false;
            effectContext.Receiver.StackHandler.AddStack(this);

            float timePassed = 0;
            float timeStarted = Time.fixedTime;
            float timeToEnd = Time.fixedTime + Duration;
            while (!RoutineCancelled && timeToEnd > Time.fixedTime)
            {
                timePassed = (Time.fixedTime - timeStarted) / Duration;

                float occuranceTimePassed = 0;
                float occuranceStart = Time.fixedTime;
                float occuranceEnd = Time.fixedTime + DelayBetweenOccurances;
                while (!RoutineCancelled && occuranceEnd > Time.fixedTime)
                {
                    occuranceTimePassed = (Time.fixedTime - timeStarted) / Duration;
                    yield return new WaitForFixedUpdate();
                }

                ApplyEffectBehaviours.ForEach(x => x.Apply(effectContext));
                StackApplicationBehaviours.ForEach(x => x.Apply(effectContext));
                yield return new WaitForFixedUpdate();
            }
            effectContext.Receiver.StackHandler.RemoveStack(this);
            DurationEffectCompletedBehaviours.ForEach(x => x.Run(effectContext));

            RoutineCancelled = false;
        }

        protected override void CancelRoutine()
        {
            RoutineCancelled = true;
        }

        public override void Apply(EffectContext effectContext)
        {
            MonobehaviourReference.Instance.StartCoroutine(TemporalRoutine(effectContext));
        }
    }

    //Wasnt sure how to abstract lerping into non-effect specific
    public class LerpTransformApplicationBehaviour : DurationEffectBehaviour
    {
        public RelativeTo.DirectionBehaviour Direction { get; set; }
        public float Distance { get; set; }

        public LerpTransformApplicationBehaviour() : base()
        {

        }

        public LerpTransformApplicationBehaviour(float duration, float distance, RelativeTo.DirectionBehaviour direction) : base(duration, new List<DurationEffectCompleted>(), new List<ApplyEffectBehaviour>())
        {
            Distance = distance;
            Direction = direction;
        }

        public LerpTransformApplicationBehaviour(float duration, float distance, RelativeTo.DirectionBehaviour direction, List<DurationEffectCompleted> completedBehaviour) : base(duration, completedBehaviour, new List<ApplyEffectBehaviour>())
        {
            Distance = distance;
            Direction = direction;
        }

        public LerpTransformApplicationBehaviour(float duration, float distance, RelativeTo.DirectionBehaviour direction, List<DurationEffectCompleted> completedBehaviour, List<DuringDurationEffect> duringBehaviour) : base(duration, completedBehaviour, duringBehaviour, new List<ApplyEffectBehaviour>() )
        {
            Distance = distance;
            Direction = direction;

        }

        public LerpTransformApplicationBehaviour(LerpTransformApplicationBehaviour original) : base(original)
        {
            Distance = original.Distance;
            Direction = original.Direction;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override DurationEffectBehaviour Clone()
        {
            return new LerpTransformApplicationBehaviour(this);
        }

        public override void Cancel()
        {
            CancelRoutine();
        }

        protected override IEnumerator TemporalRoutine(EffectContext effectContext)
        {
            Vector3 direction = Direction.GetVector(effectContext);
            EntityAdapter eA = effectContext.Sender;
            RoutineCancelled = false;
            Vector3 startingPosition = eA.transform.position;
            Vector3 endPosition = startingPosition + (direction * Distance);
            effectContext.Receiver.StackHandler.AddStack(this);

            float completionAsPercentage = 0;
            float timeStarted = Time.fixedTime;
            float timeToEnd = Time.fixedTime + Duration;
            DuringDurationEffects.ForEach(x => x.Setup(effectContext));

            Debug.Log($"Temporary - Start: {timeStarted}, End: timeToEnd {timeToEnd}");
            while (!RoutineCancelled && Time.fixedTime < timeToEnd)
            {
                completionAsPercentage = (Time.fixedTime - timeStarted) / Duration;
                eA.transform.position = Vector3.Lerp(startingPosition, endPosition, completionAsPercentage);
                DuringDurationEffects.ForEach(x => x.Run(effectContext, completionAsPercentage));
                yield return new WaitForEndOfFrame();
            }
            Debug.Log($"Temporary - Actual End: {Time.fixedTime}");

            DuringDurationEffects.ForEach(x => x.Teardown(effectContext));
            effectContext.Receiver.StackHandler.RemoveStack(this);
            eA.transform.position = endPosition;
            DurationEffectCompletedBehaviours.ForEach(x => x.Run(effectContext));
        }

        protected override void CancelRoutine()
        {
            RoutineCancelled = true;
        }

        public override void Apply(EffectContext effectContext)
        {
            MonobehaviourReference.Instance.StartCoroutine(TemporalRoutine(effectContext));
        }
    }
}