﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MAS.EffectContext;

namespace MAS
{
    public abstract class Effect : ICloneable
    {
        public static int EffectCount { get; set; }
        public static int InstanceCount { get; set; }

        public EffectContextEntity Target { get; set; }
        public int EffectId { get; set; }
        public int InstanceId { get; set; }

        public EffectContext EffectContext { get; set; }

        public Effect(EffectContextEntity target = EffectContextEntity.Receiver)
        {
            EffectId = EffectCount++;
            Target = target;
        }

        public Effect(Effect effect)
        {
            Target = effect.Target;
            InstanceId = InstanceCount++;
        }

        public void ApplyEffect(EffectContext effectContext)
        {
            EffectContext = effectContext;
            ApplyEffect();
        }

        public void RemoveEffect()
        {
            RemoveEffect_Child();
        }

        public abstract ICloneable IClone();
        public abstract Effect Clone();
        protected abstract void ApplyEffect();
        protected abstract void RemoveEffect_Child();
    }

    public class FlatDeltaHealthEffect : Effect
    {
        public Constants.delT<int> Strength { get; set; }

        public FlatDeltaHealthEffect(int strength, EffectContextEntity target = EffectContextEntity.Receiver) : base(target)
        {
            Strength = () => strength;
        }

        public FlatDeltaHealthEffect(Constants.delT<int> strength, EffectContextEntity target = EffectContextEntity.Receiver) : base(target)
        {
            Strength = strength;
        }

        public FlatDeltaHealthEffect(FlatDeltaHealthEffect effect) : base(effect)
        {
            Strength = effect.Strength;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new FlatDeltaHealthEffect(this);
        }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).FlatDeltaCurrentHealth(Strength(), EffectContext);
            //Debug.Log("Applied delta health : " + Strength) ;
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).FlatDeltaCurrentHealth(-Strength(), EffectContext);
        }
    }

    public class FlatMovementSpeedEffect : Effect
    {
        public float Strength { get; set; }

        public FlatMovementSpeedEffect(float strength) : base()
        {
            Strength = strength;
        }

        public FlatMovementSpeedEffect(FlatMovementSpeedEffect effect) : base(effect)
        {
            Strength = effect.Strength;
        }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).FlatDeltaCurrentMovementSpeed(Strength, EffectContext);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new FlatMovementSpeedEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).FlatDeltaCurrentMovementSpeed(-Strength, EffectContext);
        }
    }

    public class PercentMovementSpeedEffect : Effect
    {
        public float Strength { get; set; }

        public PercentMovementSpeedEffect(float strength) : base()
        {
            Strength = strength;
        }

        public PercentMovementSpeedEffect(PercentMovementSpeedEffect effect) : base(effect)
        {
            Strength = effect.Strength;
        }

        protected override void ApplyEffect()
        {
            EntityAdapter target = EffectContext.Get(Target);
            target.PercentDeltaCurrentMovementSpeed(target.GetBaseMovementSpeed() * (0.01f * Strength), EffectContext);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new PercentMovementSpeedEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EntityAdapter target = EffectContext.Get(Target);
            target.PercentDeltaCurrentMovementSpeed(-(target.GetBaseMovementSpeed() * (0.01f * Strength)), EffectContext);
        }
    }

    public class FlatDeltaSizeEffect : Effect
    {
        public Vector3 FlatAmount { get; set; }

        public FlatDeltaSizeEffect(float amount) : base()
        {
            FlatAmount = new Vector3(amount, amount, amount);
        }

        public FlatDeltaSizeEffect(FlatDeltaSizeEffect effect) : base(effect)
        {
            FlatAmount = effect.FlatAmount;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new FlatDeltaSizeEffect(this);
        }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).FlatDeltaCurrentSize(FlatAmount);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).FlatDeltaCurrentSize(-FlatAmount);
        }
    }

    public class UnrotatableEffect : Effect
    {
        public UnrotatableEffect() : base() { }

        public UnrotatableEffect(UnrotatableEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaIsUnrotatable(true);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new UnrotatableEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaIsUnrotatable(false);
        }
    }

    public class DisarmedEffect : Effect
    {
        public DisarmedEffect() : base() { }

        public DisarmedEffect(DisarmedEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaIsDisarmed(true);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new DisarmedEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaIsDisarmed(false);
        }
    }

    public class ImmobilisedEffect : Effect
    {
        public ImmobilisedEffect() : base() { }

        public ImmobilisedEffect(ImmobilisedEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaIsImmobilised(true);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new ImmobilisedEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaIsImmobilised(false);
        }
    }

    public class InvulnerableEffect : Effect
    {
        public InvulnerableEffect() : base()
        {
        }

        public InvulnerableEffect(InvulnerableEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaIsInvulnerable(true);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new InvulnerableEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaIsInvulnerable(false);
        }
    }

    public class DamageResistanceEffect : Effect
    {
        private int PercentResistance { get; set; }

        public DamageResistanceEffect(int percentResistance) : base()
        {
            PercentResistance = percentResistance;
        }

        public DamageResistanceEffect(DamageResistanceEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaDamageResistance(PercentResistance, EffectContext);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new DamageResistanceEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaDamageResistance(-PercentResistance, EffectContext);
        }
    }

    public class DamageMitigationEffect : Effect
    {
        private int FlatResistance { get; set; }
        public DamageMitigationEffect(int flatResistance) : base()
        {
            FlatResistance = flatResistance;
        }

        public DamageMitigationEffect(DamageMitigationEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaDamageMitigation(FlatResistance, EffectContext);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new DamageMitigationEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaDamageMitigation(-FlatResistance, EffectContext);
        }
    }

    public class UntargetableEffect : Effect
    {
        public UntargetableEffect() : base()
        {
        }

        public UntargetableEffect(UntargetableEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaIsUntargetable(true);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new UntargetableEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaIsUntargetable(false);
        }
    }

    public class UncollidableEffect : Effect
    {
        public UncollidableEffect() : base()
        {
        }

        public UncollidableEffect(UncollidableEffect effect) : base(effect) { }

        protected override void ApplyEffect()
        {
            EffectContext.Get(Target).DeltaIsUncollidable(true);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new UncollidableEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            EffectContext.Get(Target).DeltaIsUncollidable(false);
        }
    }

    public class DisplaceEffect : Effect
    {
        public RelativeTo.DirectionBehaviour Direction { get; set; }
        public DisplacementMethods.DisplacementMethod DisplaceWith { get; set; }

        public DisplaceEffect() : base() { }

        public DisplaceEffect(RelativeTo.DirectionBehaviour direction, DisplacementMethods.DisplacementMethod method) : base()
        {
            Direction = direction;
            DisplaceWith = method;
        }

        public DisplaceEffect(DisplaceEffect effect) : base(effect)
        {
            Direction = effect.Direction;
            DisplaceWith = effect.DisplaceWith;
        }

        protected override void ApplyEffect()
        {
            //EffectContext.Get(Target).StatusHandler.Displaced = true;            
            Vector3 direction = Direction.GetVector(EffectContext);
            DisplaceWith.Displace(EffectContext.Get(Target), direction);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override Effect Clone()
        {
            return new DisplaceEffect(this);
        }

        protected override void RemoveEffect_Child()
        {
            //EffectContext.Get(Target).StatusHandler.Displaced = false;
        }
    }
}