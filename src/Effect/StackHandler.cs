﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class StackHandler
    {
        public Dictionary<int, DurationEffectBehaviour> DurationEffects { get; set; }

        public int GetStackCount(MAS.DurationEffectBehaviour durationEffectBehaviour)
        {
            DurationEffectBehaviour b;
            DurationEffects.TryGetValue(durationEffectBehaviour.DurationEffectId, out b);
            int stackCount = 0;
            if (b == null) stackCount = 0;
            else stackCount = b.StackCount;
            return stackCount;
        }

        public StackHandler()
        {
            DurationEffects = new Dictionary<int, DurationEffectBehaviour>();
        }

        public void AddStack(DurationEffectBehaviour durationEffect)
        {
            durationEffect.StackCount = 1;
            DurationEffects.Add(durationEffect.DurationEffectId, durationEffect);
        }

        public void RemoveStack(DurationEffectBehaviour durationEffect)
        {
            durationEffect.StackCount = 0;
            DurationEffects.Remove(durationEffect.DurationEffectId);
        }
    }
}
