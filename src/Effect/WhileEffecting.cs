using MAS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface DuringDurationEffect : ICloneable
    {
        void Setup(EffectContext effectContext);
        void Run(EffectContext effectContext, float animationCompletionPercentage);
        void Teardown(EffectContext effectContext);

        public class PlayAnimation : DuringDurationEffect
        {
            public string AnimationName { get; set; }
            public Animator AnimatorPrefab { get; set; }
            public Animator Animator { get; set; }

            public PlayAnimation(string animationName, Animator animator)
            {
                AnimationName = animationName;
                AnimatorPrefab = animator;
            }

            public PlayAnimation(PlayAnimation original)
            {
                AnimationName = original.AnimationName;
                AnimatorPrefab = original.AnimatorPrefab;
            }

            public ICloneable IClone()
            {
                return new PlayAnimation(this);
            }

            public void Setup(EffectContext effectContext)
            {
                Animator = GameObject.Instantiate<Animator>(AnimatorPrefab);
                Animator.transform.SetParent(effectContext.Sender.transform);
                Animator.transform.localPosition = Vector3.zero;
                Animator.transform.localRotation = Quaternion.Euler(0, 0, 0);
                AnimatorPrefab.speed = 0;
            }

            public void Run(EffectContext effectContext, float animationCompletionPercentage)
            {
                if (Animator)
                {
                    Animator.Play(AnimationName, 0, animationCompletionPercentage);
                }
            }

            public void Teardown(EffectContext effectContext)
            {
                GameObject.Destroy(Animator.gameObject);
            }
        }

        public class HideGameObject : DuringDurationEffect
        {
            public string Identifier { get; set; }

            private Transform HiddenObject { get; set; }

            public HideGameObject(string identifier)
            {
                Identifier = identifier;
            }

            public HideGameObject(HideGameObject original)
            {
                Identifier = original.Identifier;
            }

            public ICloneable IClone()
            {
                return new HideGameObject(this);
            }

            public void Setup(EffectContext effectContext)
            {
                HiddenObject = effectContext.Sender.transform.Find(Identifier);

                HiddenObject.gameObject.SetActive(false);
            }

            public void Run(EffectContext effectContext, float animationCompletionPercentage)
            {
                //do nothing
            }

            public void Teardown(EffectContext effectContext)
            {
                HiddenObject.gameObject.SetActive(true);
            }
        }

        public class RunParticleSystem : DuringDurationEffect
        {
            public ParticleSystem StartUpSystem { get; set; }
            public ParticleSystem TeardownSystem { get; set; }

            public RunParticleSystem(ParticleSystem startUpSystem, ParticleSystem teardownSystem)
            {
                StartUpSystem = startUpSystem;
                TeardownSystem = teardownSystem;
            }

            public RunParticleSystem(RunParticleSystem original)
            {
                StartUpSystem = original.StartUpSystem;
                TeardownSystem = original.TeardownSystem;
            }

            public ICloneable IClone()
            {
                return new RunParticleSystem(this);
            }

            public void Run(EffectContext effectContext, float animationCompletionPercentage)
            {
                //do nothing
            }

            public void Setup(EffectContext effectContext)
            {
                ParticleSystem startup = GameObject.Instantiate<ParticleSystem>(StartUpSystem);
                startup.transform.position = effectContext.Sender.transform.position;
                GameObject.Destroy(startup, startup.main.duration + startup.main.startLifetime.constant);
            }

            public void Teardown(EffectContext effectContext)
            {
                ParticleSystem teardown = GameObject.Instantiate<ParticleSystem>(TeardownSystem);
                teardown.transform.position = effectContext.Sender.transform.position;
                GameObject.Destroy(teardown, teardown.main.duration + teardown.main.startLifetime.constant);
            }
        }
    }
}