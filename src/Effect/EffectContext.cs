﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class EffectContext : AbilityContext
    {
        public EntityAdapter Receiver { get; set; }
        public TransformMimic CastPoint { get; set; }
        public TransformMimic CollisionPoint { get; set; }
        public enum EffectContextEntity { Sender, Vessel, Receiver }
        public enum EffectContextTransform { Sender, Vessel, Receiver, ExpiryPoint, Cast, Collision }

        public EffectContext(AbilityContext abilityContext, EntityAdapter receiver, TransformMimic castLocation, TransformMimic collision) : base(abilityContext)
        {
            Receiver = receiver;
            CastPoint = castLocation;
            CollisionPoint = collision;
        }

        public EffectContext(EntityAdapter sender, EntityAdapter vessel, EntityAdapter receiver, TransformMimic castLocation, TransformMimic collision) : base(sender, vessel)
        {
            Receiver = receiver;
            CastPoint = castLocation;
            CollisionPoint = collision;
        }

        public AbilityContext GetAbilityContext()
        {
            return new AbilityContext(Sender, Vessel);
        }

        public Vector3 Get(EffectContextTransform contextType, TransformVectorTypes vectorType = TransformVectorTypes.Position)
        {
            Vector3 vector;

            switch (contextType)
            {
                case EffectContextTransform.ExpiryPoint:
                    vector = GetVector(vectorType, ExpiryPoint);
                    break;
                case EffectContextTransform.Vessel:
                    vector = GetVector(vectorType, Vessel);
                    break;
                default:
                    vector = GetVector(vectorType, Sender);
                    break;
            }

            return vector;
        }

        public static Vector3 GetVector(TransformVectorTypes vectorType, TransformMimic mimic)
        {
            Vector3 vector;

            switch (vectorType)
            {
                case TransformVectorTypes.Position:
                    vector = mimic.Position;
                    break;
                case TransformVectorTypes.Rotation:
                    vector = mimic.Rotation;
                    break;
                default:
                    vector = Vector2.zero;
                    Debug.Log("Provided vector type doesnt exist or isn't implemented");
                    break;
            }

            return vector;
        }

        public static Vector3 GetVector(TransformVectorTypes vectorType, EntityAdapter entityAdapter)
        {
            Vector3 vector;

            switch (vectorType)
            {
                case TransformVectorTypes.Position:
                    vector = entityAdapter.transform.position;
                    break;
                case TransformVectorTypes.Rotation:
                    vector = entityAdapter.transform.rotation.eulerAngles;
                    break;
                default:
                    vector = Vector2.zero;
                    Debug.Log("Provided vector type doesnt exist or isn't implemented");
                    break;
            }

            return vector;
        }

        public EntityAdapter Get(EffectContextEntity effectContextEntity)
        {
            EntityAdapter getTarget;

            switch(effectContextEntity)
            {
                case EffectContextEntity.Sender:
                    getTarget = Sender;
                    break;
                case EffectContextEntity.Vessel:
                    getTarget = Vessel;
                    break;
                case EffectContextEntity.Receiver:
                    getTarget = Receiver;
                    break;
                default:
                    getTarget = Sender;
                    Debug.LogError("EffectContextEntity provided that doesn't exist.");
                    break;
                      
            }

            return getTarget;
        }
    }
}