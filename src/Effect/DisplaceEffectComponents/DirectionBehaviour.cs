﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static MAS.VectorTypes;

namespace MAS
{
    public class RelativeTo
    {
        public abstract class DirectionBehaviour
        {
            public DirectionBehaviour()
            {
            }

            public abstract Vector3 GetVector(EffectContext effectContext);
        }

        public class Global : DirectionBehaviour
        {
            public Vector3 Offset { get; set; }

            public Global(Vector3 offset)
            {
                Offset = offset;
            }

            public override Vector3 GetVector(EffectContext effectContext)
            {
                return Offset;
            }

        }

        public abstract class Relative : DirectionBehaviour
        {
            public VectorType VectorType { get; set; }

            public Relative(VectorType vectorType)
            {
                VectorType = vectorType;
            }
        }

        public class Sender : Relative
        {
            public Sender(VectorType vectorType) : base(vectorType) { }

            public override Vector3 GetVector(EffectContext effectContext)
            {
                return VectorType.GetVector(effectContext.Sender.transform);
            }
        }

        public class SenderToReceiver : Relative
        {
            public SenderToReceiver(VectorType vectorType) : base(vectorType) { }

            public override Vector3 GetVector(EffectContext effectContext)
            {
                return VectorType.GetVector(effectContext.Sender.transform) - VectorType.GetVector(effectContext.Receiver.transform);
            }
        }

        public class Receiver : Relative
        {
            public Receiver(VectorType vectorType) : base(vectorType) { }

            public override Vector3 GetVector(EffectContext effectContext)
            {
                return VectorType.GetVector(effectContext.Receiver.transform);
            }
        }
        public class CastLocation : Relative
        {
            public CastLocation(VectorType vectorType) : base(vectorType) { }

            public override Vector3 GetVector(EffectContext effectContext)
            {
                return VectorType.GetVector(effectContext.CastPoint);
            }
        }
        public class CollisionLocation : Relative
        {
            public CollisionLocation(VectorType vectorType) : base(vectorType) { }

            public override Vector3 GetVector(EffectContext effectContext)
            {
                return VectorType.GetVector(effectContext.CollisionPoint);
            }
        }        
    }

    public class VectorTypes
    {
        public abstract class VectorType
        {
            public Vector3 Offset { get; set; }

            public VectorType(Vector3 offset) { Offset = offset; }

            public abstract Vector3 GetVector(Transform transform);
            public abstract Vector3 GetVector(TransformMimic transform);
        }

        public class Position : VectorType
        {
            public Position(Vector3 offset) : base(offset) { }

            public override Vector3 GetVector(Transform transform) { return transform.position + Offset; }
            public override Vector3 GetVector(TransformMimic transform) { return transform.Position; }
        }

        public class LookDirection : VectorType
        {
            public LookDirection() : base(new Vector3(0, 0, 0)) { }
            public LookDirection(Vector3 offset) : base(offset) { }
            public LookDirection(float offset) : base(new Vector3(0, 0, offset)) { }

            public override Vector3 GetVector(Transform transform) { return Quaternion.Euler(Offset) * transform.forward; }
            public override Vector3 GetVector(TransformMimic transform) { return Quaternion.Euler(Offset) * transform.forward; }
        }
    }
}
