﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MAS
{
    public class DisplacementMethods
    {
        public abstract class DisplacementMethod
        {
            public abstract void Displace(EntityAdapter eA, Vector3 direction);
        }

        public class Translate : DisplacementMethod
        {
            public override void Displace(EntityAdapter eA, Vector3 direction)
            {
                eA.gameObject.transform.position = direction;
            }
        }

        public class RelativeTranslate : DisplacementMethod
        {
            public override void Displace(EntityAdapter eA, Vector3 direction)
            {
                eA.gameObject.transform.position += direction;
            }
        }

        public class TranslateDistanceOverTime : DisplacementMethod
        {
            public enum State { running, cancelled, finished }
            public State state { get; set; }

            public float Distance { get; set; }
            public float Duration { get; set; }

            public TranslateDistanceOverTime(float distance, float duration)
            {
                Distance = distance;
                Duration = duration;
            }

            public override void Displace(EntityAdapter eA, Vector3 direction)
            {
                MonobehaviourReference.Instance.StartCoroutine(Translate(eA, direction * Distance, Duration));
            }

            public IEnumerator Translate(EntityAdapter eA, Vector3 direction, float duration)
            {
                state = State.running;
                float timePassed = 0;
                Vector3 startingPosition = eA.transform.position;
                Vector3 endPosition = startingPosition + direction;
                float timeStarted = Time.fixedTime;
                float timeToEnd = Time.fixedTime + Duration;

                while (state == State.running && timeToEnd > Time.fixedTime)
                {
                    timePassed = (Time.fixedTime - timeStarted) / duration;
                    eA.transform.position = Vector3.Lerp(startingPosition, endPosition, timePassed / duration);

                    yield return new WaitForEndOfFrame();
                }

                state = State.finished;
                eA.transform.position = endPosition;
            }
        }

        public class Force : DisplacementMethod
        {
            public float Magnitude { get; set; }

            public Force(float magnitude)
            {
                Magnitude = magnitude;
            }

            public override void Displace(EntityAdapter eA, Vector3 displacement)
            {
                try
                {
                    eA.GetRigidbody().AddForce(displacement * Magnitude);
                }
                catch(NullReferenceException e)
                {
                    Debug.Log($"Struck target({eA.name}) didn't have rigidbody bound. : " + e.Message);
                }
            }
        }

        public class RelativeForce : DisplacementMethod
        {
            public float Magnitude { get; set; }

            public RelativeForce(float magnitude)
            {
                Magnitude = magnitude;
            }

            public override void Displace(EntityAdapter eA, Vector3 displacement)
            {
                try
                {
                    eA.GetRigidbody().AddRelativeForce(displacement * Magnitude);
                }
                catch (NullReferenceException e)
                {
                    Debug.Log("Struck target didn't have rigidbody bound. : " + e.Message);
                }
            }
        }
    }
}
