using MAS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface DurationEffectCompleted : ICloneable
{
        void Run(EffectContext effectContext);

        public class RunAbilityCourse : DurationEffectCompleted
        {
            public AbilityCourse AbilityCourse { get; set; }

            public RunAbilityCourse(AbilityCourse abilityCourse)
            {
                AbilityCourse = abilityCourse;
            }

            public RunAbilityCourse(RunAbilityCourse original)
            {
                AbilityCourse = original.AbilityCourse.Clone();
            }

            public void Run(EffectContext effectContext)
            {
                AbilityCourse.RunCourse(effectContext);
            }

            public ICloneable IClone()
            {
                return new RunAbilityCourse(this);
            }
        }
    }
}