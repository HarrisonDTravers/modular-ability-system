﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public enum TransformVectorTypes { Position, Rotation, Scale }

    public class AbilityContext
    {
        public EntityAdapter Sender { get; set; }
        public EntityAdapter Vessel { get; set; }
        public TransformMimic ExpiryPoint { get; set; } //Location of an object that expired
        public enum AbilityContextEntities { Sender, Vessel }
        public enum AbilityContextTransforms { Sender, Vessel, ExpiryPoint }        

        public AbilityContext(EntityAdapter sender, EntityAdapter vessel)
        {
            Sender = sender;
            Vessel = vessel;
        }

        public AbilityContext(AbilityContext abilityContext)
        {
            Sender = abilityContext.Sender;
            Vessel = abilityContext.Vessel;
            ExpiryPoint = abilityContext.ExpiryPoint;
        }

        public Vector3 Get(AbilityContextTransforms contextType, TransformVectorTypes vectorType = TransformVectorTypes.Position)
        {
            Vector3 vector;

            switch(contextType)
            {
                case AbilityContextTransforms.ExpiryPoint:
                    vector = GetVector(vectorType, ExpiryPoint);
                    break;
                case AbilityContextTransforms.Vessel:
                    vector = GetVector(vectorType, Vessel);
                    break;
                default:
                    vector = GetVector(vectorType, Sender);
                    break;
            }

            return vector;
        }

        public static Vector3 GetVector(TransformVectorTypes vectorType, TransformMimic mimic)
        {
            Vector3 vector;

            switch(vectorType)
            {
                case TransformVectorTypes.Position:
                    vector = mimic.Position;
                    break;
                case TransformVectorTypes.Rotation:
                    vector = mimic.Rotation;
                    break;
                default:
                    vector = Vector3.zero;
                    Debug.Log("Provided vector type doesnt exist or isn't implemented");
                    break;
            }

            return vector;
        }

        public static Vector3 GetVector(TransformVectorTypes vectorType, EntityAdapter entityAdapter)
        {
            Vector3 vector;

            switch (vectorType)
            {
                case TransformVectorTypes.Position:
                    vector = entityAdapter.transform.position;
                    break;
                case TransformVectorTypes.Rotation:
                    vector = entityAdapter.transform.rotation.eulerAngles;
                    break;
                default:
                    vector = Vector3.zero;
                    Debug.Log("Provided vector type doesnt exist or isn't implemented");
                    break;
            }

            return vector;
        }

        public EntityAdapter Get(AbilityContextEntities effectContextEntity)
        {
            EntityAdapter getTarget;

            switch (effectContextEntity)
            {
                case AbilityContextEntities.Sender:
                    getTarget = Sender;
                    break;
                case AbilityContextEntities.Vessel:
                    getTarget = Vessel;
                    break;
                default:
                    getTarget = Sender;
                    Debug.LogError("AbilityContextEntity provided that doesn't exist.");
                    break;

            }

            return getTarget;
        }
    }

}
