﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public abstract class OnProjectileExpiryBehaviour : ICloneable
    {
        public abstract ICloneable IClone();
        public abstract void OnExpiry(Projectile projectile);
    }

    public class CastAbilityCourseOnProjectileExpiry : OnProjectileExpiryBehaviour
    {
        public AbilityCourse AbilityCourse { get; set; }

        public CastAbilityCourseOnProjectileExpiry(AbilityCourse abilityCourse)
        {
            AbilityCourse = abilityCourse;
        }

        public override void OnExpiry(Projectile projectile)
        {
            projectile.AbilityContext.ExpiryPoint = new TransformMimic(projectile.ProjectileReference.transform);
            AbilityCourse.RunCourse(projectile.AbilityContext);
        }

        public CastAbilityCourseOnProjectileExpiry Clone()
        {
            return new CastAbilityCourseOnProjectileExpiry (AbilityCourse.Clone());
        }

        public override ICloneable IClone()
        {
            return Clone();
        }
    }

    public class ParticleSystemAtPositionOnProjectileExpiryBehaviour : OnProjectileExpiryBehaviour
    {
        //Replace all these with LineRendererBehaviours
        public ParticleSystem ParticleSystem { get; set; }
        public AbilityContext.AbilityContextTransforms ContextType { get; set; }

        public ParticleSystemAtPositionOnProjectileExpiryBehaviour(AbilityContext.AbilityContextTransforms contextType, ParticleSystem particleSystem)
        {
            ParticleSystem = particleSystem;
            ContextType = contextType;
        }

        public override void OnExpiry(Projectile projectile)
        {
            ParticleSystem instance = MonoBehaviour.Instantiate<ParticleSystem>(ParticleSystem);
            instance.gameObject.transform.position = projectile.AbilityContext.Get(ContextType, TransformVectorTypes.Position);

            MonobehaviourReference.Instance.StartCoroutine(DestroyAfterLifetime(instance));
        }

        public IEnumerator DestroyAfterLifetime(ParticleSystem particleSystem)
        {
            bool lifeEnded = false;
            float timePassed = 0;
            while (!lifeEnded)
            {
                if (timePassed >= particleSystem.main.duration + particleSystem.main.startLifetime.constantMax) lifeEnded = true;
                timePassed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            GameObject.Destroy(particleSystem.gameObject);
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public ParticleSystemAtPositionOnProjectileExpiryBehaviour Clone()
        {
            return new ParticleSystemAtPositionOnProjectileExpiryBehaviour(ContextType, ParticleSystem);
        }
    }
}
