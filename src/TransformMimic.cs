﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class TransformMimic
    {
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public Vector3 forward { get; set; } //Which way the object is facing

        public TransformMimic(Vector3 position, Vector3 vector, Vector3 rotation)
        {
            Position = position;
            forward = vector;
            Rotation = rotation;
        }

        public TransformMimic(Transform transform)
        {
            Position = transform.position;
            Rotation = transform.rotation.eulerAngles;
        }
    }
}
