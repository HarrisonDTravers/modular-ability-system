﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class UnitMatrix
    {
        /*       |Self|Ally|Neut|Enem| 
        Hero     | 0  | 1  | 2  | 3  |
        Minion   | 4  | 5  | 6  | 7  |
        Summon   | 8  | 9  | 10 | 11 |
        Structure| 12 | 13 | 14 | 15 |*/

        public bool[][] unitMatrix;

        public UnitMatrix(bool[][] unitMatrix)
        {
            this.unitMatrix = unitMatrix;
        }

        public bool CanTarget(EntityAdapter caster, EntityAdapter receiver)
        {
            return unitMatrix[(int)receiver.EntityType][(int)caster.Group.AffinityTowards(receiver.Group)];
        }

        public UnitMatrix Clone()
        {
            return new UnitMatrix((bool[][])unitMatrix.Clone());
        }

        public static UnitMatrix NeutralAndEnemy()
        {
            bool[][] unitMatrixArray = new bool[][]{
                                    /*Enem   Neut   Ally  Self*/
            new bool[] /*Hero*/     { true, true, false, false },
            new bool[] /*Minion*/   { true, true, false, false },
            new bool[] /*Summon*/   { true, true, false, false },
            new bool[] /*Structure*/{ true, true, false, false },
        };

            return new UnitMatrix(unitMatrixArray);
        }

        public static UnitMatrix SelfAndAlly()
        {
            bool[][] unitMatrixArray = new bool[][]{
                                    /*Enem   Neut   Ally  Self*/
            new bool[] /*Hero*/     { false, false, true, true },
            new bool[] /*Minion*/   { false, false, true, true },
            new bool[] /*Summon*/   { false, false, true, true },
            new bool[] /*Structure*/{ false, false, true, true }
        };

            return new UnitMatrix(unitMatrixArray);
        }

        public static UnitMatrix SelfBeastling()
        {
            bool[][] unitMatrixArray = new bool[][]{
                                    /*Enem   Neut   Ally  Self*/
            new bool[] /*Hero*/     { false, false, true, true },
            new bool[] /*Minion*/   { false, false, true, false },
            new bool[] /*Summon*/   { false, false, true, false },
            new bool[] /*Structure*/{ false, false, true, false }
        };

            return new UnitMatrix(unitMatrixArray);
        }

        public static UnitMatrix EnemyBeastling()
        {
            bool[][] unitMatrixArray = new bool[][]
            {
                                        /*Enem   Neut   Ally  Self*/
                new bool[] /*Hero*/     { true, false, false, false },
                new bool[] /*Minion*/   { false, false, false, false },
                new bool[] /*Summon*/   { false, false, false, false },
                new bool[] /*Structure*/{ false, false, false, false }
            };

            return new UnitMatrix(unitMatrixArray);
        }

        public static UnitMatrix Enemy()
        {
            bool[][] unitMatrixArray = new bool[][]
            {
                                        /*Enem   Neut   Ally  Self*/
                new bool[] /*Hero*/     { true, false, false, false },
                new bool[] /*Minion*/   { true, false, false, false },
                new bool[] /*Summon*/   { true, false, false, false },
                new bool[] /*Structure*/{ true, false, false, false }
            };

            return new UnitMatrix(unitMatrixArray);
        }
    }
}
