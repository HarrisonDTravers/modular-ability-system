﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface IDeliveryProjectileTranslationBehaviour : ICloneable //TranslationBehaviour for short
    {
        void Setup(ProjectileReference projectileRef);
        void Perform(Vector3 translation);
    }

    public class FaceChildrenForwardTranslationBehaviour : IDeliveryProjectileTranslationBehaviour
    {
        private Vector3 Offset { get; set; }
        private ProjectileReference ProjectileRef { get; set; }
        public FaceChildrenForwardTranslationBehaviour()
        {
            Offset = new Vector3(-90, 0, 0);
        }

        public FaceChildrenForwardTranslationBehaviour(Vector3 offset)
        {
            Offset = offset;
        }

        public void Setup(ProjectileReference projectileRef)
        {
            ProjectileRef = projectileRef;

            for (int ii = 0; ii < ProjectileRef.gameObject.transform.childCount; ii++)
            {
                Quaternion rotation = ProjectileRef.gameObject.transform.GetChild(ii).rotation;
                ProjectileRef.gameObject.transform.GetChild(ii).rotation = Quaternion.Euler(rotation.x + Offset.x, rotation.y + Offset.y, rotation.z + Offset.z);
            }
        }

        public void Perform(Vector3 translation)
        {
            Vector3 curPos = ProjectileRef.gameObject.transform.position;
            Vector3 nextPos = ProjectileRef.gameObject.transform.position + translation;
            Vector3 direction = (curPos - nextPos).normalized;
            for (int ii = 0; ii < ProjectileRef.gameObject.transform.childCount; ii++)
            {
                float angle = Vector2.Angle(Vector2.up, new Vector2(direction.x, direction.z));
                if(direction.x < 0)
                    angle = -angle;
                ProjectileRef.gameObject.transform.GetChild(ii).gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, angle, 0) + Offset);
            }

        }

        public ICloneable IClone()
        {
            return new FaceChildrenForwardTranslationBehaviour(Offset);
        }
    }

    public class RotateTranslationBehaviour : IDeliveryProjectileTranslationBehaviour
    {
        private Vector3 Offset { get; set; }
        private Vector3 Angle { get; set; }
        private ProjectileReference ProjectileRef { get; set; }
        public RotateTranslationBehaviour()
        {

        }

        public RotateTranslationBehaviour(Vector3 offset, Vector3 angle)
        {
            Offset = offset;
            Angle = angle;
        }

        public void Setup(ProjectileReference projectileRef)
        {
            ProjectileRef = projectileRef;
            for (int ii = 0; ii < ProjectileRef.gameObject.transform.childCount; ii++)
            {
                Quaternion rotation = ProjectileRef.gameObject.transform.GetChild(ii).rotation;
                ProjectileRef.gameObject.transform.GetChild(ii).rotation = Quaternion.Euler(rotation.x + Offset.x, rotation.y + Offset.y, rotation.z + Offset.z);
            }
        }

        public void Perform(Vector3 translation)
        {
            Vector3 curPos = ProjectileRef.gameObject.transform.position;
            Vector3 nextPos = ProjectileRef.gameObject.transform.position + translation;
            Vector3 direction = nextPos - curPos;
            for (int ii = 0; ii < ProjectileRef.gameObject.transform.childCount; ii++)
            {
                ProjectileRef.gameObject.transform.GetChild(ii).Rotate(Angle);
            }
        }

        public ICloneable IClone()
        {
            return new RotateTranslationBehaviour(Offset, Angle);
        }
    }
}