﻿using MAS;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace MAS
{
    public interface IProjectileBehaviour : ICloneable
    {
        void Run(Projectile projectile);
    }

    public class NoProjectileBehaviour : IProjectileBehaviour
    {
        public void Run(Projectile projectile) { }

        public ICloneable IClone()
        {
            return new NoProjectileBehaviour();
        }
    }

    public abstract class ColliderProjectileBehaviour : IProjectileBehaviour
    {
        protected Projectile Projectile { get; set; }
        protected List<IOnCollisionBehaviour> OnCollisionBehaviours { get; set; }
        protected UnitMatrix UnitMatrix { get; set; }

        public ColliderProjectileBehaviour(List<IOnCollisionBehaviour> onCollisionBehaviours, UnitMatrix unitMatrix)
        {
            UnitMatrix = unitMatrix;
            OnCollisionBehaviours = onCollisionBehaviours;
        }

        public ColliderProjectileBehaviour(IOnCollisionBehaviour onCollisionBehaviour, UnitMatrix unitMatrix)
        {
            UnitMatrix = unitMatrix;
            OnCollisionBehaviours = new List<IOnCollisionBehaviour>() { onCollisionBehaviour };
        }

        protected void Projectile_CollisionEvent(EntityAdapter entity, TransformMimic collisionTransform)
        {
            if (UnitMatrix.CanTarget(Projectile.AbilityContext.Sender, entity))
            {
                bool runBehaviours = true;
                foreach (IOnCollisionBehaviour b in OnCollisionBehaviours)
                    if (!b.Check(Projectile, entity, collisionTransform))
                        runBehaviours = false;//If even one is false, then dont run behaviours

                if(runBehaviours)
                    foreach (IOnCollisionBehaviour b in OnCollisionBehaviours)
                        b.Run(Projectile, entity, collisionTransform);
            }
        }

        //Called before the collision event (this function sets up collision event to be called by next collision)
        //Calls base function first, then Run in child is called via abstract
        public void Run(Projectile projectile)
        {
            Projectile = projectile;
            projectile.ProjectileReference.CollisionEvent += Projectile_CollisionEvent;
            Run();
        }

        public abstract ICloneable IClone();
        protected abstract void Run();
    }

    public class SphereColliderProjectileBehaviour : ColliderProjectileBehaviour
    {
        public Constants.delT<float> Radius { get; set; }
        SphereCollider collider;

        public SphereColliderProjectileBehaviour(float radius, UnitMatrix unitMatrix, params IOnCollisionBehaviour[] onCollisionBehaviours) : base(onCollisionBehaviours.ToList(), unitMatrix)
        {
            Radius = () => radius;
        }

        public SphereColliderProjectileBehaviour(Constants.delT<float> radius, UnitMatrix unitMatrix, params IOnCollisionBehaviour[] onCollisionBehaviours) : base(onCollisionBehaviours.ToList(), unitMatrix)
        {
            Radius = radius;
        }

        public SphereColliderProjectileBehaviour(float radius, List<IOnCollisionBehaviour> onCollisionBehaviours, UnitMatrix unitMatrix) : base(onCollisionBehaviours, unitMatrix)
        {
            Radius = () => radius;
        }

        public SphereColliderProjectileBehaviour(float radius, EffectPackage effectPackage, UnitMatrix unitMatrix) : base(new List<IOnCollisionBehaviour>() { new ApplyEffectPackageOnCollisionbehaviour(effectPackage), new DestroyAfterCollisionsBehaviour() }, unitMatrix)
        {
            Radius = () => radius;
        }

        public SphereColliderProjectileBehaviour(SphereColliderProjectileBehaviour clone) : base(Utilities.CloneList<IOnCollisionBehaviour>(clone.OnCollisionBehaviours), clone.UnitMatrix)
        {
            Radius = clone.Radius;
        }

        //Called by parent(inheritence) via abstract
        protected override void Run()
        {
            collider = this.Projectile.ProjectileReference.gameObject.AddComponent<SphereCollider>();
            collider.isTrigger = true;
            collider.radius = Radius();
        }

        public override ICloneable IClone()
        {
            return new SphereColliderProjectileBehaviour(this);
        }
    }

    public class CircleColliderProjectileBehaviour : ColliderProjectileBehaviour
    {
        public float Radius { get; set; }
        CircleCollider2D collider;

        public CircleColliderProjectileBehaviour(float radius, UnitMatrix unitMatrix, params IOnCollisionBehaviour[] onCollisionBehaviours) : base(onCollisionBehaviours.ToList(), unitMatrix)
        {
            Radius = radius;
        }

        public CircleColliderProjectileBehaviour(float radius, List<IOnCollisionBehaviour> onCollisionBehaviours, UnitMatrix unitMatrix) : base(onCollisionBehaviours, unitMatrix)
        {
            Radius = radius;
        }

        public CircleColliderProjectileBehaviour(float radius, EffectPackage effectPackage, UnitMatrix unitMatrix) : base(new List<IOnCollisionBehaviour>() { new ApplyEffectPackageOnCollisionbehaviour(effectPackage), new DestroyAfterCollisionsBehaviour() }, unitMatrix)
        {
            Radius = radius;
        }

        //Called by parent(inheritence) via abstract
        protected override void Run()
        {
            collider = this.Projectile.ProjectileReference.gameObject.AddComponent<CircleCollider2D>();
            collider.isTrigger = true;
            collider.radius = Radius;
        }

        public override ICloneable IClone()
        {
            return new CircleColliderProjectileBehaviour(Radius, Utilities.CloneList(OnCollisionBehaviours), UnitMatrix.Clone());
        }
    }

    public interface IOnCollisionBehaviour : ICloneable
    {
        bool Check(Projectile projectile, EntityAdapter entityAdapter, TransformMimic collisionTransform);
        void Run(Projectile projectile, EntityAdapter entityAdapter, TransformMimic collisionTransform);
    }

    public class DestroyAfterCollisionsBehaviour : IOnCollisionBehaviour
    {
        Constants.delT<int> MaxCollisions;
        int CollisionCount { get; set; }

        public DestroyAfterCollisionsBehaviour(Constants.delT<int> maxCollisions = null) 
        {
            if (maxCollisions == null) maxCollisions = () => { return 1; };
            MaxCollisions = maxCollisions; 
            CollisionCount = 0; 
        }

        public ICloneable IClone()
        {
            return new DestroyAfterCollisionsBehaviour(MaxCollisions);
        }

        public bool Check(Projectile projectile, EntityAdapter entityAdapter, TransformMimic collisionTransform)
        {
            return CollisionCount < MaxCollisions();
        }

        public void Run(Projectile projectile, EntityAdapter entityAdapter, TransformMimic collisionTransform)
        {
            CollisionCount++;
            if (CollisionCount >= MaxCollisions())
                projectile.Destroy();
        }
    }

    public class ApplyEffectPackageOnCollisionbehaviour : IOnCollisionBehaviour
    {
        private List<EffectPackage> EffectPackages { get; set; }
        private EffectContext.EffectContextEntity Target { get; set; }

        public bool Check(Projectile projectile, EntityAdapter entityAdapter, TransformMimic collisionTransform)
        {
            return true;
        }

        public ApplyEffectPackageOnCollisionbehaviour(EffectPackage effectPackage, EffectContext.EffectContextEntity target = EffectContext.EffectContextEntity.Receiver)
        {
            EffectPackages = new List<EffectPackage>() { effectPackage };
            Target = target;
        }

        public ApplyEffectPackageOnCollisionbehaviour(EffectContext.EffectContextEntity target = EffectContext.EffectContextEntity.Receiver, params EffectPackage[] effectPackages)
        {
            EffectPackages = new List<EffectPackage>(effectPackages);
            Target = target;
        }

        public ApplyEffectPackageOnCollisionbehaviour(List<EffectPackage> effectPackages, EffectContext.EffectContextEntity target = EffectContext.EffectContextEntity.Receiver)
        {
            EffectPackages = new List<EffectPackage>(effectPackages);
            Target = target;
        }

        public ICloneable IClone()
        {
            return new ApplyEffectPackageOnCollisionbehaviour(Utilities.CloneList(EffectPackages), Target);
        }

        public void Run(Projectile projectile, EntityAdapter collisionEntity, TransformMimic collisionTransform)
        {
            EntityAdapter effectTarget;
            switch(Target)
            {
                case EffectContext.EffectContextEntity.Sender:
                    effectTarget = projectile.AbilityContext.Sender;
                    break;
                case EffectContext.EffectContextEntity.Vessel:
                    effectTarget = projectile.AbilityContext.Vessel;
                    break;
                case EffectContext.EffectContextEntity.Receiver:
                    effectTarget = collisionEntity;
                    break;
                default:
                    effectTarget = null;
                    break;
            }

            foreach (EffectPackage package in EffectPackages)
                package.Apply(new EffectContext(projectile.AbilityContext, effectTarget, projectile.StartInfo, collisionTransform));
        }
    }

    public class DestroyAfterDurationProjectileBehaviour : IProjectileBehaviour
    {
        public Constants.delT<float> Duration { get; set; }
        public float RemainingDuration { get; set; }
        public List<OnProjectileExpiryBehaviour> OnExpiryBehaviours { get; set; }

        public DestroyAfterDurationProjectileBehaviour(float duration)
        {
            Duration = () => duration;
            OnExpiryBehaviours = new List<OnProjectileExpiryBehaviour>();
        }

        public DestroyAfterDurationProjectileBehaviour(Constants.delT<float> duration)
        {
            Duration = duration;
            OnExpiryBehaviours = new List<OnProjectileExpiryBehaviour>();
        }

        public DestroyAfterDurationProjectileBehaviour(float duration, params OnProjectileExpiryBehaviour[] onExpiryBehaviours)
        {
            Duration = () => duration;
            OnExpiryBehaviours = new List<OnProjectileExpiryBehaviour>(onExpiryBehaviours);
        }

        public DestroyAfterDurationProjectileBehaviour(float duration, List<OnProjectileExpiryBehaviour> onExpiryBehaviours)
        {
            Duration = () => duration;
            OnExpiryBehaviours = new List<OnProjectileExpiryBehaviour>(onExpiryBehaviours);
        }

        public DestroyAfterDurationProjectileBehaviour(DestroyAfterDurationProjectileBehaviour clone)
        {
            Duration = clone.Duration;
            OnExpiryBehaviours = clone.OnExpiryBehaviours;
        }

        public ICloneable IClone()
        {
            return new DestroyAfterDurationProjectileBehaviour(this);
        }

        public void Run(Projectile projectile)
        {
            MonobehaviourReference.Instance.StartCoroutine(DestroyOnZeroDuration(projectile));
            projectile.OnDestroy += CancelDestroy;
        }

        public IEnumerator DestroyOnZeroDuration(Projectile projectile)
        {
            RemainingDuration = Duration();

            while (RemainingDuration > 0)
            {
                RemainingDuration -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            try
            {
                foreach (OnProjectileExpiryBehaviour expiryBehaviour in OnExpiryBehaviours)
                    expiryBehaviour.OnExpiry(projectile);
                GameObject.Destroy(projectile.ProjectileReference.gameObject);
            }
            catch (MissingReferenceException) { /*Do nothing, it's already destroyed*/ }
        }

        public void CancelDestroy()
        {
            MonobehaviourReference.Instance.StopCoroutine("DestroyOnZeroDuration");
        }
    }

    public class SpriteRendererProjectileBehaviour : IProjectileBehaviour
    {
        public Sprite Sprite { get; set; }
        private SpriteRenderer SpriteRenderer { get; set; }
        public Vector3 RotationOffset { get; set; }
        public Constants.delT<float> Scale { get; set; }

        public SpriteRendererProjectileBehaviour(Sprite sprite, Constants.delT<float> scale = null)
        {
            this.Sprite = sprite;
            this.RotationOffset = new Vector3(0, 0, 0);
            if (scale == null) scale = () => { return 1; };
            Scale = scale;
        }

        public SpriteRendererProjectileBehaviour(SpriteRendererProjectileBehaviour clone)
        {
            this.Sprite = clone.Sprite;
            this.RotationOffset = clone.RotationOffset;
            this.Scale = clone.Scale;
        }

        public ICloneable IClone()
        {
            return new SpriteRendererProjectileBehaviour(this);
        }

        public void Run(Projectile projectile)
        {
            SpriteRenderer SpriteRenderer = new GameObject("ProjectileSprite").AddComponent<SpriteRenderer>();
            SpriteRenderer.sprite = Sprite;
            SpriteRenderer.gameObject.transform.SetParent(projectile.ProjectileReference.gameObject.transform);
            SpriteRenderer.transform.Rotate(RotationOffset);
            SpriteRenderer.transform.localPosition = Vector3.zero;
            SpriteRenderer.transform.localScale = Vector3.one * Scale();
        }
    }

    public class StartAtVesselPositionProjectileBehaviour : IProjectileBehaviour
    {
        public ICloneable IClone()
        {
            return new StartAtVesselPositionProjectileBehaviour();
        }

        public void Run(Projectile projectile)
        {
            projectile.ProjectileReference.gameObject.transform.position = projectile.AbilityContext.Vessel.gameObject.transform.position;
            projectile.StartInfo = new TransformMimic(projectile.AbilityContext.Vessel.gameObject.transform.position, projectile.AbilityContext.Vessel.gameObject.transform.forward, projectile.AbilityContext.Vessel.gameObject.transform.rotation.eulerAngles);
        }
    }
}