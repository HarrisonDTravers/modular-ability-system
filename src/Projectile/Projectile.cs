﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MAS
{
    public class Projectile : ICloneable
    {
        public ProjectileTransportBehaviour DeliveryProjectileBehaviour { get; set; }
        public List<IProjectileBehaviour> ProjectileBehaviours { get; set; }
        public AbilityContext AbilityContext { get; set; }
        public ProjectileReference ProjectileReference { get; set; }
        public TransformMimic StartInfo { get; set; }
        public static int projectileLayer = LayerMask.NameToLayer("Projectile");

        public event Constants.delVoid OnDestroy;

        public Projectile(List<IProjectileBehaviour> projectileBehaviours)
        {
            this.ProjectileBehaviours = projectileBehaviours;
        }

        public Projectile(params IProjectileBehaviour[] projectileBehaviour)
        {
            this.ProjectileBehaviours = projectileBehaviour.ToList();
        }

        public Projectile Instantiate(AbilityContext abilityContext, ProjectileTransportBehaviour DeliveryProjectileBehaviour)
        {
            Projectile projectile = new Projectile(Utilities.CloneList(ProjectileBehaviours));
            projectile.ProjectileReference = new GameObject("Projectile").AddComponent<ProjectileReference>();
            projectile.ProjectileReference.Projectile = this;
            projectile.ProjectileReference.gameObject.layer = projectileLayer;
            projectile.AbilityContext = abilityContext;
            projectile.DeliveryProjectileBehaviour = DeliveryProjectileBehaviour;
            projectile.RunBehaviours();

            return this;
        }

        public void RunBehaviours()
        {
            DeliveryProjectileBehaviour.Deliver(this);
            foreach (IProjectileBehaviour b in ProjectileBehaviours)
                b.Run(this);
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public Projectile Clone()
        {
            return new Projectile(Utilities.CloneList(ProjectileBehaviours));
        }

        public void Destroy()
        {
            MonoBehaviour.Destroy(ProjectileReference.gameObject);
            if (OnDestroy != null) OnDestroy.Invoke();
        }
    }
}