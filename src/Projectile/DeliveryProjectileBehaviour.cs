﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public abstract class ProjectileTransportBehaviour : ICloneable
    {
        public Constants.delT<float> ProjectileSpeed { get; set; }
        public List<IDeliveryProjectileTranslationBehaviour> TranslationBehaviours { get; set; }

        public ProjectileTransportBehaviour(float projectileSpeed)
        {
            ProjectileSpeed = () => projectileSpeed;
            TranslationBehaviours = new List<IDeliveryProjectileTranslationBehaviour> { new FaceChildrenForwardTranslationBehaviour() };
        }

        public ProjectileTransportBehaviour(Constants.delT<float> projectileSpeed)
        {
            ProjectileSpeed = projectileSpeed;
            TranslationBehaviours = new List<IDeliveryProjectileTranslationBehaviour> { new FaceChildrenForwardTranslationBehaviour() };
        }

        public ProjectileTransportBehaviour(Constants.delT<float> projectileSpeed, List<IDeliveryProjectileTranslationBehaviour> translationBehaviours)
        {
            ProjectileSpeed = projectileSpeed;
            TranslationBehaviours = translationBehaviours;
        }

        public abstract void Deliver(Projectile projectile);
        public abstract ProjectileTransportBehaviour Clone();
        public abstract ICloneable IClone();
    }

    public class TranslateInDirectionDeliveryProjectileBehaviour : ProjectileTransportBehaviour
    {
        public Vector3 Direction { get; set; }
        public Rigidbody2D Rigidbody;

        public TranslateInDirectionDeliveryProjectileBehaviour(Vector3 direction, float projectileSpeed) : base(projectileSpeed)
        {
            this.Direction = direction.normalized; //ensure directions are unit directions
        }

        public TranslateInDirectionDeliveryProjectileBehaviour(Vector3 direction, Constants.delT<float> projectileSpeed) : base(projectileSpeed)
        {
            this.Direction = direction.normalized; //ensure directions are unit directions
        }

        public TranslateInDirectionDeliveryProjectileBehaviour(Vector3 direction, Constants.delT<float> projectileSpeed, List<IDeliveryProjectileTranslationBehaviour> translationBehaviours) : base(projectileSpeed, translationBehaviours)
        {
            this.Direction = direction.normalized; //ensure directions are unit directions
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override ProjectileTransportBehaviour Clone()
        {
            return new TranslateInDirectionDeliveryProjectileBehaviour(Direction, ProjectileSpeed, Utilities.CloneList<IDeliveryProjectileTranslationBehaviour>(TranslationBehaviours));
        }

        public override void Deliver(Projectile projectile)
        {
            TranslationBehaviours.ForEach(b => b.Setup(projectile.ProjectileReference));
            TranslateInDirection(projectile); //Run this frame because next coroutine starts on next frame
            projectile.ProjectileReference.StartCoroutine(CoroutineTranslateInDirection(projectile));
        }

        public IEnumerator CoroutineTranslateInDirection(Projectile projectile)
        {
            while (true)
            {
                TranslateInDirection(projectile);
                yield return new WaitForEndOfFrame();
            }
        }

        public void TranslateInDirection(Projectile projectile)
        {

            Vector3 translation = Direction * ProjectileSpeed() * Time.deltaTime;
            TranslationBehaviours.ForEach(b => b.Perform(translation));
            projectile.ProjectileReference.transform.Translate(translation, Space.World);
        }
    }

    public class TranslateTowardsTargetProjectileBehaviour : ProjectileTransportBehaviour
    {
        public EntityAdapter Target { get; set; }

        //TargetObserverBehaviours, destroy game object when target is destroyed. (observs entity.die())

        public TranslateTowardsTargetProjectileBehaviour(EntityAdapter target, float projectileSpeed) : base(projectileSpeed)
        {
            Target = target;
        }

        public TranslateTowardsTargetProjectileBehaviour(EntityAdapter target, Constants.delT<float> projectileSpeed) : base(projectileSpeed)
        {
            Target = target;
        }

        public TranslateTowardsTargetProjectileBehaviour(EntityAdapter target, Constants.delT<float> projectileSpeed, List<IDeliveryProjectileTranslationBehaviour> translationBehaviours) : base(projectileSpeed, translationBehaviours)
        {
            Target = target;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override ProjectileTransportBehaviour Clone()
        {
            return new TranslateTowardsTargetProjectileBehaviour(Target, ProjectileSpeed, Utilities.CloneList<IDeliveryProjectileTranslationBehaviour>(TranslationBehaviours));
        }

        public override void Deliver(Projectile projectile)
        {
            TranslationBehaviours.ForEach(b => b.Setup(projectile.ProjectileReference));
            projectile.ProjectileReference.gameObject.GetComponent<ProjectileReference>().StartCoroutine(TranslateTowardsTarget(projectile));
        }

        public IEnumerator TranslateTowardsTarget(Projectile projectile)
        {
            while (Target != null)
            {
                Vector3 directionOfTarget = (Target.gameObject.transform.position - projectile.ProjectileReference.gameObject.transform.position).normalized;
                Vector3 translation = directionOfTarget * ProjectileSpeed() * Time.deltaTime;
                TranslationBehaviours.ForEach(b => b.Perform(translation));
                projectile.ProjectileReference.gameObject.transform.Translate(translation);
                yield return new WaitForEndOfFrame();
            }
            if (Target == null)
                projectile.Destroy();

        }
    }
}