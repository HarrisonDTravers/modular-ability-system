﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public class ProjectileReference : MonoBehaviour
    {
        public Projectile Projectile { get; set; }
        public event Constants.delVoidInputEntityAdapter_TransformMimic CollisionEvent;

        private void OnTriggerEnter(Collider collision)
        {
            EntityAdapter entity = collision.GetComponent<Collider>().GetComponent<EntityAdapter>();
            if (entity != null)
                CollisionEvent?.Invoke(entity, new TransformMimic(collision.transform.position, collision.transform.forward, collision.transform.rotation.eulerAngles));
        }
    }
}