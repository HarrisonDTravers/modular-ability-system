﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface ICloneable
    {
        ICloneable IClone();
    }

    public class Utilities
    {
        public static List<T> CloneList<T>(List<T> list) where T : ICloneable
        {
            List<T> newList = new List<T>();
            list.ForEach(x =>
            {
            //Debug.Log(typeof(T) + " : " + x.GetType());
            newList.Add((T)x.IClone());
            });
            return newList;
        }

        public static Vector2 AngleToVector(Vector2 p, float angle)
        {
            Vector2 temp;
            angle = -angle;
            temp.x = p.x * Mathf.Cos(angle) + p.y * Mathf.Sin(angle);
            temp.y = -p.x * Mathf.Sin(angle) + p.y * Mathf.Cos(angle);
            return temp;
        }
    }
}

