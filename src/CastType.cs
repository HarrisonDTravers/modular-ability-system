﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public abstract class CastType : ICloneable
    {
        public List<IOnCastTypeCastBehaviour> CastTypeBehaviours { get; set; }

        public CastType()
        {
            CastTypeBehaviours = new List<IOnCastTypeCastBehaviour>();
        }

        public CastType(List<IOnCastTypeCastBehaviour> castTypeBehaviours)
        {
            CastTypeBehaviours = castTypeBehaviours;
        }

        public void Cast(AbilityContext abilityContext)
        {
            foreach(IOnCastTypeCastBehaviour behaviour in CastTypeBehaviours)
            {
                behaviour.OnCast(abilityContext);
            }

            Cast_Child(abilityContext);
        }

        protected abstract void Cast_Child(AbilityContext abilityContext);
        public abstract ICloneable IClone();
    }

    public class TargettedCastType : CastType
    {
        public TargetBehaviour TargetBehaviour { get; set; }
        public float DelayAfterTargetting { get; set; }
        public ITargettedDeliveryBehaviour DeliveryBehaviour { get; set; }

        public TargettedCastType(ITargettedDeliveryBehaviour deliveryBehaviour, TargetBehaviour targetBehaviour)
        {
            DeliveryBehaviour = deliveryBehaviour;
            TargetBehaviour = targetBehaviour;
        }

        public TargettedCastType(ITargettedDeliveryBehaviour deliveryBehaviour, TargetBehaviour targetBehaviour, params IOnCastTypeCastBehaviour[] castTypeBehaviours) : base(new List<IOnCastTypeCastBehaviour>(castTypeBehaviours))
        {
            DeliveryBehaviour = deliveryBehaviour;
            TargetBehaviour = targetBehaviour;
        }

        public TargettedCastType(ITargettedDeliveryBehaviour deliveryBehaviour, TargetBehaviour targetBehaviour, List<IOnCastTypeCastBehaviour> castTypeBehaviours) : base(castTypeBehaviours)
        {
            DeliveryBehaviour = deliveryBehaviour;
            TargetBehaviour = targetBehaviour;
        }

        public TargettedCastType(ITargettedDeliveryBehaviour deliveryBehaviour, TargetBehaviour targetBehaviour, float delayAfterTargetting, List<IOnCastTypeCastBehaviour> castTypeBehaviours) : base(castTypeBehaviours)
        {
            DeliveryBehaviour = deliveryBehaviour;
            TargetBehaviour = targetBehaviour;
            DelayAfterTargetting = delayAfterTargetting;
        }

        protected override void Cast_Child(AbilityContext abilityContext)
        {
            List<EntityAdapter> targets = TargetBehaviour.GetTargets(abilityContext);
            MonobehaviourReference.Instance.StartCoroutine(CastAfterDelay(abilityContext, targets, DelayAfterTargetting));
        }

        public IEnumerator CastAfterDelay(AbilityContext abilityContext, List<EntityAdapter> targets, float delay)
        {
            bool hasWaited = false;
            while(!hasWaited)
            {
                hasWaited = true;
                yield return new WaitForSeconds(delay);
            }

            foreach (EntityAdapter target in targets)
            {
                DeliveryBehaviour.SendDelivery(abilityContext, target);
            }
        }

        public override ICloneable IClone()
        {
            return new TargettedCastType(DeliveryBehaviour.Clone(), TargetBehaviour.Clone(), Utilities.CloneList<IOnCastTypeCastBehaviour>(CastTypeBehaviours));
        }
    }

    public class AimedCastType : CastType
    {
        public IAimedDeliveryBehaviour DeliveryBehaviour { get; set; }
        public Vector3 RotationOffset { get; set; }

        public AimedCastType(IAimedDeliveryBehaviour deliveryBehaviour)
        {
            DeliveryBehaviour = deliveryBehaviour;
        }
        public AimedCastType(IAimedDeliveryBehaviour deliveryBehaviour, Vector3 rotationOffset)
        {
            DeliveryBehaviour = deliveryBehaviour;
            RotationOffset = rotationOffset;
        }
        public AimedCastType(IAimedDeliveryBehaviour deliveryBehaviour, params IOnCastTypeCastBehaviour[] castTypeBehaviour) : base(new List<IOnCastTypeCastBehaviour>(castTypeBehaviour))
        {
            DeliveryBehaviour = deliveryBehaviour;
        }
        public AimedCastType(IAimedDeliveryBehaviour deliveryBehaviour, Vector3 rotationOffset, List<IOnCastTypeCastBehaviour> castTypeBehaviours) : base(castTypeBehaviours)
        {
            DeliveryBehaviour = deliveryBehaviour;
            RotationOffset = rotationOffset;
        }

        protected override void Cast_Child(AbilityContext abilityContext)
        {
            if(abilityContext.Sender != null)
                DeliveryBehaviour.SendDelivery(abilityContext, Quaternion.Euler(RotationOffset) * abilityContext.Sender.GetAimDirection());
        }

        public override ICloneable IClone()
        {
            return new AimedCastType(DeliveryBehaviour.Clone(), RotationOffset, Utilities.CloneList<IOnCastTypeCastBehaviour>(CastTypeBehaviours));
        }
    }

    public class CollisionCastType : CastType
    {
        public ITargettedDeliveryBehaviour DeliveryBehaviour { get; set; }
        public AbilityContext AbilityContext { get; set; }
        public UnitMatrix UnitMatrix { get; set; }
        public int CollisionLimit { get; set; }
        public float CastDuration { get; set; }

        private float RemainingDuration;
        private float RemainingCollisions;
        private bool ListeningToEvent;

        public CollisionCastType(int collisionLimit, float castDuration, UnitMatrix unitMatrix, ITargettedDeliveryBehaviour deliveryBehaviour)
        {
            CollisionLimit = collisionLimit;
            CastDuration = castDuration;
            UnitMatrix = unitMatrix;
            DeliveryBehaviour = deliveryBehaviour;
        }

        public CollisionCastType(int collisionLimit, float castDuration, UnitMatrix unitMatrix, ITargettedDeliveryBehaviour deliveryBehaviour, params IOnCastTypeCastBehaviour[] castTypeBehaviour) : base(new List<IOnCastTypeCastBehaviour>(castTypeBehaviour))
        {
            CollisionLimit = collisionLimit;
            CastDuration = castDuration;
            UnitMatrix = unitMatrix;
            DeliveryBehaviour = deliveryBehaviour;
        }

        public CollisionCastType(int collisionLimit, float castDuration, UnitMatrix unitMatrix, ITargettedDeliveryBehaviour deliveryBehaviour, List<IOnCastTypeCastBehaviour> castTypeBehaviours) : base(castTypeBehaviours)
        {
            CollisionLimit = collisionLimit;
            CastDuration = castDuration;
            UnitMatrix = unitMatrix;
            DeliveryBehaviour = deliveryBehaviour;
        }

        public CollisionCastType(CollisionCastType collisionCastType)
        {
            CollisionLimit = collisionCastType.CollisionLimit;
            CastDuration = collisionCastType.CastDuration;
            UnitMatrix = collisionCastType.UnitMatrix.Clone();
            DeliveryBehaviour = collisionCastType.DeliveryBehaviour.Clone();
            CastTypeBehaviours = collisionCastType.CastTypeBehaviours;
        }

        protected override void Cast_Child(AbilityContext abilityContext)
        {
            AbilityContext = abilityContext;
            RemainingDuration = CastDuration;
            RemainingCollisions = CollisionLimit;
            ListeningToEvent = true;
            MonobehaviourReference.Instance.StartCoroutine(Timer());
            StartListeningForCollisions();
        }

        public void Collision(EntityAdapter collisionTarget)
        {
            if (UnitMatrix.CanTarget(AbilityContext.Sender, collisionTarget) && RemainingCollisions > 0 )
            {
                DeliveryBehaviour.SendDelivery(AbilityContext, collisionTarget);
                RemainingCollisions--;
                if(RemainingCollisions <= 0)
                {
                    StopListeningForCollisions();
                }
            }            
        }

        public IEnumerator Timer()
        {
            while(RemainingDuration > 0 && ListeningToEvent)
            {
                RemainingDuration -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            if(ListeningToEvent)
            {
                StopListeningForCollisions();
            }
        }

        public void StartListeningForCollisions()
        {
            AbilityContext.Vessel.OnCollision += Collision;
        }

        public void StopListeningForCollisions()
        {
            AbilityContext.Vessel.OnCollision -= Collision;
        }

        public override ICloneable IClone()
        {
            return new CollisionCastType(this);
        }
    }
}