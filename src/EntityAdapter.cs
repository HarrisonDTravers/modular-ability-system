﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public enum EntityType { Hero = 0, Minion = 1, Summon = 2, Structure = 3 }

    public class EntityAdapter : MonoBehaviour
    {
        public StackHandler StackHandler { get; set; }
        public EntityType EntityType { get; set; } //Todo: Needs to be replaced by an object system like Group
        public Group Group { get; set; }
        public AbilityHandler AbilityBehaviour { get; set; }
        public object Entity { get; set; }
        
        private new Rigidbody rigidbody { get; set; }
        public Rigidbody Rigidbody { get { if (rigidbody == null) { rigidbody = gameObject.GetComponent<Rigidbody>(); } return rigidbody; } set { rigidbody = value; } }

        public Constants.delVector3 GetAimDirection;

        public void Initialize(object entity, EntityType entityType, Group group, AbilityHandler abilityBehaviour)
        {
            Entity = entity;
            EntityType = entityType;
            Group = group;
            AbilityBehaviour = abilityBehaviour;
            StackHandler = new StackHandler();
        }

        //HEALTH
        //Events
        //Flat change in max health
                public event Constants.delVoidInputInt_EffectContext OnFlatDeltaMaxHealth;
                //Percent change in max health
                public event Constants.delVoidInputInt_EffectContext OnPercentDeltaMaxHealth;
                //Flat change in current health
                public event Constants.delVoidInputInt_EffectContext OnFlatDeltaCurrentHealth;
                //Percent change in current health
                public event Constants.delVoidInputInt_EffectContext OnPercentDeltaCurrentHealth;
                //Change in damage resistance
                public event Constants.delVoidInputInt_EffectContext OnDeltaDamageResistance;
                //Change in damage mitigation
                public event Constants.delVoidInputInt_EffectContext OnDeltaDamageMitigation;
            //Functions
                public void FlatDeltaMaxHealth(int flatValue, EffectContext context) { OnFlatDeltaMaxHealth?.Invoke(flatValue, context); }
                public void PercentDeltaMaxHealth(int percentValue, EffectContext context) { OnPercentDeltaMaxHealth?.Invoke(percentValue, context); }
                public void FlatDeltaCurrentHealth(int flatValue, EffectContext context) { OnFlatDeltaCurrentHealth?.Invoke(flatValue, context); }
                public void PercentDeltaCurrentHealth(int percentValue, EffectContext context) { OnPercentDeltaCurrentHealth?.Invoke(percentValue, context); }
                public void DeltaDamageResistance(int percentValue, EffectContext context) { OnDeltaDamageResistance?.Invoke(percentValue, context); }
                public void DeltaDamageMitigation(int flatValue, EffectContext context) { OnPercentDeltaCurrentHealth?.Invoke(flatValue, context); }
        //Out event? Currently events only ggo from EntityAdapter to entity but there are none that go from entity to entityadapter so that what this is
                public event Constants.delVoidInputEntityAdapter_EffectContext OnEntityDie;
                public void EntityDied(EffectContext effectContext) { OnEntityDie?.Invoke(this, effectContext); }


        //MOVEMENT
            //Events
                //Flat change in current movement speed
                    public event Constants.delVoidInputFloat_EffectContext OnFlatDeltaCurrentMovementSpeed;
                    //Percentage change in current movement speed
                    public event Constants.delVoidInputFloat_EffectContext OnPercentDeltaCurrentMovementSpeed;
                    //Flat change in base movement speed
                    public event Constants.delVoidInputFloat_EffectContext OnFlatDeltaBaseMovementSpeed;
                    //Percentage change in base movement speed
                    public event Constants.delVoidInputFloat_EffectContext OnPercentDeltaBaseMovementSpeed;
                //Functions
                    public void FlatDeltaBaseMovementSpeed(float flatValue, EffectContext context) { OnFlatDeltaBaseMovementSpeed?.Invoke(flatValue, context); }
                    public void PercentDeltaBaseMovementSpeed(float percentValue, EffectContext context) { OnPercentDeltaBaseMovementSpeed?.Invoke(percentValue, context); }
                    public void FlatDeltaCurrentMovementSpeed(float flatValue, EffectContext context) { OnFlatDeltaCurrentMovementSpeed?.Invoke(flatValue, context); }
                    public void PercentDeltaCurrentMovementSpeed(float percentValue, EffectContext context) { OnPercentDeltaCurrentMovementSpeed?.Invoke(percentValue, context); }
                //Delegates
                    public Constants.delFloat GetBaseMovementSpeed { get; set; }
                    public Constants.delRigidbody GetRigidbody { get; set; }

        //TRANSFORM
        //Events
        //Flat change in current movement speed
        public event Constants.delVoidInputVector3 OnFlatDeltaCurrentSize;
            //Functions
                public void FlatDeltaCurrentSize(Vector3 flatValue) { OnFlatDeltaCurrentSize?.Invoke(flatValue); }
                
            //ROTATION 
                //Events
                    public event Constants.delVoidInputBool OnDeltaIsUnrotatable;
                //Functions
                    public void DeltaIsUnrotatable(bool isUnrotatable) { OnDeltaIsUnrotatable?.Invoke(isUnrotatable); }

            //DISARMED
                //Events
                    public event Constants.delVoidInputBool OnDeltaIsDisarmed;
                //Functions
                    public void DeltaIsDisarmed(bool isDisabled) { OnDeltaIsDisarmed?.Invoke(isDisabled); }

            //IMMOBILISED
                //Event
                    public event Constants.delVoidInputBool OnDeltaIsImmobilised;
                //Functions
                    public void DeltaIsImmobilised(bool isImmobilised) { OnDeltaIsImmobilised?.Invoke(isImmobilised); }

            //INVULNERABLE
                //Event
                    public event Constants.delVoidInputBool OnDeltaIsInvulnerable;
                //Functions
                    public void DeltaIsInvulnerable(bool isInvulnerable) { OnDeltaIsInvulnerable?.Invoke(isInvulnerable); }

            //TARGETABLE
                //Event
                    public event Constants.delVoidInputBool OnDeltaIsUntargetable;
                //Functions
                    public void DeltaIsUntargetable(bool isUntargetable) { OnDeltaIsUntargetable?.Invoke(isUntargetable); }

            //TARGETABLE
                //Event
                public event Constants.delVoidInputBool OnDeltaIsUncollidable;
                //Functions
                public void DeltaIsUncollidable(bool isUncollidable) { OnDeltaIsUncollidable?.Invoke(isUncollidable); }

        //COLLISION
        //Event
        public event Constants.delVoidInputEntityAdapter OnCollision;
                //Functions
                public void OnCollisionEnter(Collision collision)
                {
                    EntityAdapter eA = collision.collider.gameObject.GetComponent<EntityAdapter>();
                    if(eA != null) { OnCollision?.Invoke(eA); }            
                }
    }
}
