using MAS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityVariables
{
    public Dictionary<string, Variable> variables = new();

    public void Set<T>(string name, T value)
    {
        if (!variables.TryGetValue(name, out Variable var))
        {
            variables.Add(name, new Variable() { Type = value.GetType(), Value = value });
        }
        else
        {
            var.Value = (T)value;
        }
    }

    public bool TrySet<T>(string name, T value)
    {
        if (!variables.TryGetValue(name, out Variable var))
        {
            variables.Add(name, new Variable() { Type = value.GetType(), Value = value });
            return true;
        }
        else if (var.Type != typeof(T))
            return false;

        var.Value = (T)var.Value;

        return true;
    }

    public bool TryGet<T>(string name, out T value)
    {
        value = default;

        if (!variables.TryGetValue(name, out Variable var))
            return false;
        if (var.Type != typeof(T))
            return false;

        value = (T) var.Value;

        return true;
    }

    public T Get<T>(string name)
    {
        return (T)variables[name].Value;
    }

    public Constants.delT<T> GetRef<T>(string name)
    {
        Variable var = variables[name];
        return () => { return (T)var.Value; };
    }
}

public class Variable
{
    public System.Type Type { get; set; }
    public object Value { get; set; }
}