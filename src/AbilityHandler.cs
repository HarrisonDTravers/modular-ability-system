﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MAS
{
    public class AbilityHandler
    {
        public class AbilityCastEvent { public event Constants.delVoid OnAbilityCast; public void Cast() { if (OnAbilityCast != null) OnAbilityCast(); } }
        public AbilityContext AbilityContext { get; set; }
        private Dictionary<int,Ability> Abilities { get; set; }
        public Dictionary<int,AbilityCastEvent> CastEvents { get; set; }
        public bool Disarmed { get; set; }

        public void Initialize(List<Ability> abilities, AbilityContext abilityContext)
        {
            AbilityContext = abilityContext;
            CastEvents = new Dictionary<int, AbilityCastEvent>();
            Abilities = new Dictionary<int, Ability>();

            for (int ii = 0; ii < abilities.Count; ii++)
                SetAbility(abilities[ii], ii);
        }

        public void Initialize(Ability[] abilities, AbilityContext abilityContext)
        {
            Initialize(abilities.ToList(), abilityContext);
        }

        public void IsDisarmed(bool boolean)
        {
            Disarmed = boolean;
        }

        public void SetAbility(Ability ability, int index)
        {
            /*if(Abilities.TryGetValue(index, out Ability abilityAtIndex))
                abilityAtIndex.Disable*/ //Need to implement disabling abilities to stop passives from continuing

            CastEvents[index] = new AbilityCastEvent();
            ability.AbilityConfiguration.Configure(AbilityContext, ability, index);
            Abilities[index] = ability;
        }

        public Ability GetAbility(int index)
        {
            return Abilities[index];
        }

        public Dictionary<int,Ability> GetAbilities()
        {
            return Abilities;
        }

        public AbilityHandler Clone()
        {
            return new AbilityHandler();
        }


        public void CastAbility(int i)
        {
            if (!Disarmed)
            {
                try
                {
                    CastEvents[i].Cast();
                }
                catch (KeyNotFoundException e) { /*Using an ability slot that doesnt have an ability in it*/}
            }
        }
    }
}