﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public interface IOnInstantDeliveryBehaviour : ICloneable
    {
        void Run(EffectContext effectContext);
    }

    public class LineRendererOnInstantDeliveryBehaviour : IOnInstantDeliveryBehaviour
    {
        //Replace all these with LineRendererBehaviours
        public Color Color { get; set; }
        public float Width { get; set; }
        public float LifeTime { get; set; }

        public LineRendererOnInstantDeliveryBehaviour(Color color, float width, float lifeTime)
        {
            Color = color;
            Width = width;
            LifeTime = lifeTime;
        }

        public void Run(EffectContext effectContext)
        {
            LineRenderer lineRenderer = new GameObject("LineRenderer").AddComponent<LineRenderer>();
            lineRenderer.material = Constants.UI.SpritesDefault;
            lineRenderer.startColor = this.Color;
            lineRenderer.endColor = this.Color;
            lineRenderer.startWidth = Width;
            lineRenderer.endWidth = Width;
            lineRenderer.SetPositions(new Vector3[] { effectContext.Vessel.gameObject.transform.position, effectContext.Receiver.gameObject.transform.position });

            MonobehaviourReference.Instance.StartCoroutine(DestroyAfterLifetime(lineRenderer));
        }

        public IEnumerator DestroyAfterLifetime(LineRenderer lineRenderer)
        {
            bool lifeEnded = false;
            float timePassed = 0;
            while (!lifeEnded)
            {
                if (timePassed >= LifeTime) lifeEnded = true;
                timePassed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            GameObject.Destroy(lineRenderer.gameObject);
        }

        public ICloneable IClone()
        {
            return new LineRendererOnInstantDeliveryBehaviour(new Color(Color.r, Color.g, Color.b, Color.a), Width, LifeTime);
        }
    }

    /// <summary>
    /// Line renderer will render between and track two entities every frame.
    /// </summary>
    public class TrackingLineRendererOnInstantDeliveryBehaviour : IOnInstantDeliveryBehaviour
    {
        //Replace all these with LineRendererBehaviours
        public Color Color { get; set; }
        public float Width { get; set; }
        public float LifeTime { get; set; }
        public EffectContext.EffectContextEntity LineStart;
        public EffectContext.EffectContextEntity LineEnd;

        private Transform lineStartTransform;
        private Transform lineEndTransform;

        public TrackingLineRendererOnInstantDeliveryBehaviour(Color color, float width, float lifeTime, EffectContext.EffectContextEntity lineStart, EffectContext.EffectContextEntity lineEnd)
        {
            Color = color;
            Width = width;
            LifeTime = lifeTime;
            LineStart = lineStart;
            LineEnd = lineEnd;
        }

        public void Run(EffectContext effectContext)
        {
            LineRenderer lineRenderer = new GameObject("LineRenderer").AddComponent<LineRenderer>();
            lineRenderer.material = Constants.UI.SpritesDefault;
            lineRenderer.startColor = this.Color;
            lineRenderer.endColor = this.Color;
            lineRenderer.startWidth = Width;
            lineRenderer.endWidth = Width;            
            lineStartTransform = effectContext.Get(LineStart).transform;
            lineEndTransform = effectContext.Get(LineEnd).transform;

            MonobehaviourReference.Instance.StartCoroutine(TrackForDuration(lineRenderer));
        }

        public IEnumerator TrackForDuration(LineRenderer lineRenderer)
        {
            bool durationEnded = false;
            float timePassed = 0;
            while (!durationEnded && lineStartTransform != null && lineEndTransform != null)
            {
                if (timePassed >= LifeTime) durationEnded = true;
                timePassed += Time.deltaTime;
                lineRenderer.SetPositions(new Vector3[] { lineStartTransform.position, lineEndTransform.position });
                yield return new WaitForEndOfFrame();
            }
            GameObject.Destroy(lineRenderer.gameObject);
        }

        public ICloneable IClone()
        {
            return new LineRendererOnInstantDeliveryBehaviour(new Color(Color.r, Color.g, Color.b, Color.a), Width, LifeTime);
        }
    }

    public class ParticleOnEntityOnInstantDeliveryBehaviour : IOnInstantDeliveryBehaviour
    {
        //Replace all these with LineRendererBehaviours
        public ParticleSystem ParticleSystem { get; set; }
        public EffectContext.EffectContextEntity EffectContextEntity { get; set; }
        public float DistanceFromEntity { get; set; }

        public ParticleOnEntityOnInstantDeliveryBehaviour(EffectContext.EffectContextEntity effectContextEntity, ParticleSystem particleSystem)
        {
            ParticleSystem = particleSystem;
            EffectContextEntity = effectContextEntity;
        }

        public ParticleOnEntityOnInstantDeliveryBehaviour(EffectContext.EffectContextEntity effectContextEntity, ParticleSystem particleSystem, float distanceFromEntity)
        {
            ParticleSystem = particleSystem;
            EffectContextEntity = effectContextEntity;
            DistanceFromEntity = distanceFromEntity;
        }

        public void Run(EffectContext effectContext)
        {
            ParticleSystem instance = MonoBehaviour.Instantiate<ParticleSystem>(ParticleSystem);
            instance.gameObject.transform.SetParent(effectContext.Get(EffectContextEntity).transform);

            float rotation = effectContext.Get(EffectContextEntity).transform.rotation.y; //y coorinate is rotation in 3D space
            Vector2 rotationAsUnitVector2D = Utilities.AngleToVector(Vector3.up * DistanceFromEntity, Mathf.Deg2Rad * rotation).normalized;
            Vector3 boxPosition = effectContext.Get(EffectContextEntity).transform.position + new Vector3(rotationAsUnitVector2D.x, 0, rotationAsUnitVector2D.y);

            instance.gameObject.transform.localPosition = Vector3.zero;

            MonobehaviourReference.Instance.StartCoroutine(DestroyAfterLifetime(instance));
        }

        public IEnumerator DestroyAfterLifetime(ParticleSystem particleSystem)
        {
            bool lifeEnded = false;
            float timePassed = 0;
            while (!lifeEnded)
            {
                if (timePassed >= particleSystem.main.duration + particleSystem.main.startLifetime.constantMax) lifeEnded = true;
                timePassed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            GameObject.Destroy(particleSystem.gameObject);
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public ParticleOnEntityOnInstantDeliveryBehaviour Clone()
        {
            return new ParticleOnEntityOnInstantDeliveryBehaviour(EffectContextEntity, ParticleSystem);
        }
    }

    public class ParticleAtPositionOnInstantDeliveryBehaviour : IOnInstantDeliveryBehaviour
    {
        //Replace all these with LineRendererBehaviours
        public ParticleSystem ParticleSystem { get; set; }
        public EffectContext.EffectContextTransform EffectContextTransform { get; set; }

        public ParticleAtPositionOnInstantDeliveryBehaviour(EffectContext.EffectContextTransform effectContextTransform, ParticleSystem particleSystem)
        {
            ParticleSystem = particleSystem;
            EffectContextTransform = effectContextTransform;
        }

        public void Run(EffectContext effectContext)
        {
            ParticleSystem instance = MonoBehaviour.Instantiate<ParticleSystem>(ParticleSystem);
            instance.gameObject.transform.position = effectContext.Get(EffectContextTransform, TransformVectorTypes.Position);

            MonobehaviourReference.Instance.StartCoroutine(DestroyAfterLifetime(instance));
        }

        public IEnumerator DestroyAfterLifetime(ParticleSystem particleSystem)
        {
            bool lifeEnded = false;
            float timePassed = 0;
            while (!lifeEnded)
            {
                if (timePassed >= particleSystem.main.duration + particleSystem.main.startLifetime.constantMax) lifeEnded = true;
                timePassed += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            GameObject.Destroy(particleSystem.gameObject);
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public ParticleAtPositionOnInstantDeliveryBehaviour Clone()
        {
            return new ParticleAtPositionOnInstantDeliveryBehaviour(EffectContextTransform, ParticleSystem);
        }
    }
}