﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MAS
{
    public class Ability : ICloneable, id_dictionary.IDEntry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Sprite Icon { get; set; }
        public AbilityVariables AbilityVariables { get; set; }

        public List<CastBlocker> CastBlockersAddedOnCast { get; set; }
        public AbilityConfiguration AbilityConfiguration { get; set; }
        public List<OnAbilityCast> OnAbilityCast { get; set; }

        private List<CastBlocker> castBlockers;
        public void AddCastBlocker(CastBlocker blocker) { castBlockers.Add(blocker); if (OnCastBlockerAdded != null) OnCastBlockerAdded(blocker); }
        public void RemoveCastBlocker(CastBlocker blocker) { castBlockers.Remove(blocker); if (OnCastBlockerRemoved != null) OnCastBlockerRemoved(blocker); }
        public event Constants.delVoidInputCastBlocker OnCastBlockerAdded;
        public event Constants.delVoidInputCastBlocker OnCastBlockerRemoved;

        public Ability(float cooldown)
        {
            CastBlockersAddedOnCast = new List<CastBlocker>() { new CooldownCastBlocker(cooldown) };
        }

        public Ability(int id, string name, string destription, Sprite icon, AbilityConfiguration abilityConfiguration, AbilityVariables abilityVariables = null, params CastBlocker[] castBlockersAddedOnCast)
        {
            Id = id;
            Name = name;
            Description = destription;
            Icon = icon;
            AbilityConfiguration = abilityConfiguration;
            CastBlockersAddedOnCast = new List<CastBlocker>(castBlockersAddedOnCast);
            OnAbilityCast = new List<OnAbilityCast>();
            AbilityVariables = abilityVariables;

            castBlockers = new List<CastBlocker>();
        }

        //For ease of access, assumes you want active ability and a cooldown
        public Ability(int id, string name, string destription, Sprite icon, float cooldown, AbilityCourse abilityCourse)
        {
            Id = id;
            Name = name;
            Description = destription;
            Icon = icon;
            CastBlockersAddedOnCast = new List<CastBlocker>() { new CooldownCastBlocker(cooldown) };
            AbilityConfiguration = new ActiveAbilityConfiguration(abilityCourse);
            OnAbilityCast = new List<OnAbilityCast>();

            castBlockers = new List<CastBlocker>();
        }

        //For ease of access, assumes you want active ability, a cooldown 1 or more OnAbilityCast
        public Ability(int id, string name, string destription, Sprite icon, float cooldown, AbilityCourse abilityCourse, params OnAbilityCast[] onAbilityCast)
        {
            Id = id;
            Name = name;
            Description = destription;
            Icon = icon;
            CastBlockersAddedOnCast = new List<CastBlocker>() { new CooldownCastBlocker(cooldown) };
            AbilityConfiguration = new ActiveAbilityConfiguration(abilityCourse);
            OnAbilityCast = onAbilityCast.ToList();

            castBlockers = new List<CastBlocker>();
        }

        public Ability(Ability ability)
        {
            Id = ability.Id;
            Name = ability.Name;
            Description = ability.Description;
            Icon = ability.Icon;
            CastBlockersAddedOnCast = Utilities.CloneList<CastBlocker>(ability.CastBlockersAddedOnCast);
            AbilityConfiguration = ability.AbilityConfiguration.Clone();
            AbilityVariables = ability.AbilityVariables;
            OnAbilityCast = Utilities.CloneList<OnAbilityCast>(ability.OnAbilityCast);

            castBlockers = new List<CastBlocker>();
        }

        public void Cast(AbilityContext abilityContext)
        {
            if (castBlockers.Count <= 0)
            {
                AbilityConfiguration.AbilityCourse.RunCourse(abilityContext);
                OnAbilityCast.ForEach(x => x.Run(abilityContext));
                foreach (CastBlocker castBlocker in CastBlockersAddedOnCast)
                    castBlocker.Block(this);
            }
        }

        public ICloneable IClone()
        {
            return Clone();
        }

        public Ability Clone()
        {
            return new Ability(this);
        }
    }
}