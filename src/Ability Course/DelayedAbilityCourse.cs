using MAS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Temporal meaning the ability course is dependant on time
public abstract class TemporalAbilityCourse : AbilityCourse
{
    protected List<ITemporalObserver> TemporalObservers { get; set; }

    public event Constants.delVoid OnSuccesfullyComplete; //Called if the temporal event completes without being cancelled.
    public bool Cancelled { get; set; }

    public TemporalAbilityCourse()
    {
        TemporalObservers = new List<ITemporalObserver>();
    }

    public void SuccesfullyComplete()
    {
        OnSuccesfullyComplete?.Invoke();
    }
}

public interface ITemporalObserver
{
    public void SetupObserver(TemporalAbilityCourse temporalAbilityCourse, AbilityContext abilityContext);
}

public class CancelWhenSenderDies : ITemporalObserver
{
    public void SetupObserver(TemporalAbilityCourse temporalAbilityCourse, AbilityContext abilityContext)
    {
        Constants.delVoidInputEntityAdapter_EffectContext callback = null;
            callback = (EntityAdapter ea, EffectContext ec) => { 
            temporalAbilityCourse.Cancelled = true; abilityContext.Sender.OnEntityDie -= callback; 
        };

        abilityContext.Sender.OnEntityDie += callback;

        temporalAbilityCourse.OnSuccesfullyComplete += () => { abilityContext.Sender.OnEntityDie -= callback; };
    }
}