﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MAS
{
    public abstract class AbilityCourse : ICloneable
    {
        public abstract void RunCourse(AbilityContext abilityContext);
        public abstract AbilityCourse Clone();
        public abstract ICloneable IClone();
    }

    public class SingleAbilityCourse : AbilityCourse
    {
        public List<CastType> AbilityTypes { get; set; }

        public SingleAbilityCourse(List<CastType> abilityTypes)
        {
            AbilityTypes = abilityTypes;
        }
        public SingleAbilityCourse(params CastType[] abilityType)
        {
            AbilityTypes = abilityType.ToList();
        }

        public SingleAbilityCourse(float cooldown, params CastType[] abilityType)
        {
            AbilityTypes = abilityType.ToList();
        }

        public override void RunCourse(AbilityContext abilityContext)
        {
            AbilityTypes.ForEach(x => x.Cast(abilityContext));
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override AbilityCourse Clone()
        {
            return new SingleAbilityCourse(Utilities.CloneList(AbilityTypes));
        }
    }

    public class RepeatingAbilityCourse : TemporalAbilityCourse
    {
        public float Duration { get; set; }
        public Constants.delT<float> TickDurationGetter { get; set; }
        public float RemainingDuration { get; set; }
        public List<CastType> AbilityTypes { get; set; }

        public RepeatingAbilityCourse(float duration, Constants.delT<float> tickDurationGetter, List<CastType> abilityTypes)
        {
            Duration = duration;
            TickDurationGetter = tickDurationGetter;
            AbilityTypes = abilityTypes;
        }

        public RepeatingAbilityCourse(float duration, float tickDuration, List<CastType> abilityTypes)
        {
            Duration = duration;
            TickDurationGetter = () => { return tickDuration; };
            AbilityTypes = abilityTypes;
        }
        public RepeatingAbilityCourse(float duration, float tickDuration, CastType abilityType)
        {
            Duration = duration;
            TickDurationGetter = () => { return tickDuration; };
            AbilityTypes = new List<CastType>() { abilityType };
        }

        public override void RunCourse(AbilityContext abilityContext)
        {
            TemporalObservers.ForEach(observer => observer.SetupObserver(this, abilityContext));
            MonobehaviourReference.Instance.StartCoroutine(RepeatAbility(abilityContext));
        }

        public IEnumerator RepeatAbility(AbilityContext abilityContext)
        {
            float timeToEnd = Time.fixedTime + Duration;

            while (!Cancelled && timeToEnd > Time.fixedTime)
            {
                float timeToEndTick = Time.fixedTime + TickDurationGetter();
                try
                {
                    AbilityTypes.ForEach(x => x.Cast(abilityContext));
                }
                catch(MissingReferenceException e) //Maybe target or sender died
                {
                    Cancelled = true;
                }
                
                while (!Cancelled && timeToEndTick > Time.fixedTime)
                {
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForEndOfFrame();
            }

            float newTime = Time.fixedTime;

            if (!Cancelled)
                SuccesfullyComplete();

            Cancelled = false;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override AbilityCourse Clone()
        {
            return new RepeatingAbilityCourse(Duration, TickDurationGetter, Utilities.CloneList(AbilityTypes));
        }
    }

    public class ParallelAbilityCourse : AbilityCourse
    {
        public List<AbilityCourse> AbilityCourses { get; set; }

        public ParallelAbilityCourse(List<AbilityCourse> abilityCourses)
        {
            AbilityCourses = abilityCourses;
        }
        public ParallelAbilityCourse(params AbilityCourse[] abilityCourses)
        {
            AbilityCourses = abilityCourses.ToList();
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override AbilityCourse Clone()
        {
            return new ParallelAbilityCourse(AbilityCourses);
        }

        public override void RunCourse(AbilityContext abilityContext)
        {
            foreach (AbilityCourse aC in AbilityCourses)
            {
                aC.RunCourse(abilityContext);
            }
        }
    }

    public class DelayedAbilityCourse : TemporalAbilityCourse
    {
        public AbilityCourse AbilityCourseAfterDelay { get; set; }
        public float Delay { get; set; }
        public float RemainingDelay { get; set; }

        public DelayedAbilityCourse(AbilityCourse abilityCourseAfterDelay, float delay)
        {
            AbilityCourseAfterDelay = abilityCourseAfterDelay;
            Delay = delay;
        }

        public override ICloneable IClone()
        {
            return Clone();
        }

        public override AbilityCourse Clone()
        {
            return new DelayedAbilityCourse(AbilityCourseAfterDelay, Delay);
        }

        public override void RunCourse(AbilityContext abilityContext)
        {
            TemporalObservers.ForEach(observer => observer.SetupObserver(this, abilityContext));
            MonobehaviourReference.Instance.StartCoroutine(DelayCouroutine(abilityContext));
        }

        public IEnumerator DelayCouroutine(AbilityContext abilityContext)
        {
            RemainingDelay = Delay;

            while (!Cancelled && RemainingDelay > 0)
            {
                RemainingDelay -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }

            try
            {
                AbilityCourseAfterDelay.RunCourse(abilityContext);
            }
            catch (MissingReferenceException e) //Maybe target or sender died
            {
                Cancelled = true;
            }

            if (!Cancelled)
                SuccesfullyComplete();
        }
    }
}