using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAS
{
    public partial interface OnAbilityCast : ICloneable
    {
        public void Run(AbilityContext abilityContext);
    }
}
